#!/usr/bin/python

#The MIT License (MIT)
# 
#Copyright (c) 2016, Ocean Exploration Trust
#Copyright (c) 2016, J. Ian S. Vaughn
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

# External dependencies
from PyQt4 import QtGui, QtCore, uic
import yaml

# Custom dependencies
import NGcore.pyguibits as guibits
import NGcore.QtLogger as Log
import NavServe as navserve

# Python dependencies
import sys, os, signal
import glob, json, traceback
import datetime, time, collections

# Internal dependencies
import Ctd
import Probe

TIME_FORMAT = '%Y-%b-%d %H:%M:%S'

class Worker(QtCore.QThread):
    processingDone = QtCore.pyqtSignal(bool, 'QString')

    def __init__(self, parent=None):
        super(Worker, self).__init__(parent)
        self._running = True
        self._mutex = QtCore.QMutex()
        self._waitCond = QtCore.QWaitCondition()
        self._cmdQueue = collections.deque()
        self._processes = {}

    def __del__(self):
        self._running = False
        self.wait()

    def addProcess(self, name, process):
        self._processes[name] = process

    @QtCore.pyqtSlot(str, str, list)
    def runProcess(self, obj, func, args):
        self._mutex.lock()
        self._cmdQueue.append((obj, func, args))
        self._mutex.unlock()
        self._waitCond.wakeAll()

    def run(self):
        while self._running:
            self._mutex.lock()
            if len(self._cmdQueue) == 0:
                self._waitCond.wait(self._mutex)
            if len(self._cmdQueue) > 0:
                obj, func, args = self._cmdQueue.popleft()
            else:
                continue
            self._mutex.unlock()

            Log.log('[worker] Running: Object \"%s\", Command \"%s\"' % (obj, func), Log.VERB_INFO)

            failed = False
            try:
                self.executeCmd(obj, func, args)
            except Exception as e:
                Log.log('[worker] Processing FAILED!', Log.VERB_ERROR)
                Log.logException('[worker]', Log.VERB_ERROR)
                self.processingDone.emit(False, str(e))
                failed = True
            if not failed:
                self.processingDone.emit(True, 'Success!')

    def executeCmd(self, obj, func, args):
        if obj is None:
            self._processes['uctd'].run()
            self._processes['proc'].run()
            self._processes['uctd'].updateCasts()
            return

        if obj not in self._processes.keys():
            raise ValueError('Unknown process!')

        if not hasattr(self._processes[obj], func):
            raise ValueError('Object had no attribute (method, probably) named \"%s\"' % func)
        if args is not None:
            getattr(self._processes[obj], func)(*args)
        else:
            getattr(self._processes[obj], func)()


class UctdModel(QtCore.QObject):
    statusChanged = QtCore.pyqtSignal('QString')
    probeStateUpdate = QtCore.pyqtSignal(dict)

    def __init__(self, config, controller, parent=None):
        super(UctdModel, self).__init__(parent)
        self._controller = controller
        self._blue_id = config['sync']['bluetooth_id']
        self._dataCfg = config['layout']
        self._config = config
        self.loadState()
        self._probe = Probe.Probe(self._blue_id)

        self._navserve = navserve.Client(config['navserve']['addr'], int(config['navserve']['port']))
        self._cruiseId = 'NAXXX'
    
    def getCruiseId(self):
        try:
            ret = self._navserve.getCruiseId()
            Log.log('[uctd-model] Got Cruise ID from server: \"%s\"' % ret, Log.VERB_INFO)
            return ret
        except:
            Log.log('[uctd-model] Unable to get Cruise ID from server!', Log.VERB_ERROR)
            Log.logException('[uctd-model]', Log.VERB_ERROR)
        
        if self._controller.cruiseId:
            Log.log('[uctd-model] Returning controller cruise ID (from file).  NOT RECOMMENDED!', Log.VERB_WARN)
            return self._controller.cruiseId

        Log.log('[uctd-model] No cruies ID', Log.VERB_ERROR)
        return None


    def loadState(self):
        self._state = {'last_sync': '',
                'probe_state': {
                        'voltage_value': '?', 'voltage_pct': '?',
                        'data_value': '?',    'data_pct': '?',
                        'cast_value': '?',    'cast_pct': '?'},
                'probe_flags': {}
                } 
        logfile = os.path.join(self._controller.logDir, 'probe.json')
        Log.log('[uctd-model] Loading probe state from \"%s\"' % logfile, Log.VERB_DEBUG)
        if not os.path.isfile(logfile):
            Log.log('[uctd-model] Probe state file \"%s\" does not exist!' % logfile, Log.VERB_WARN)
            return

        try:
            newState = json.load(open(logfile, 'r'))
            if newState['last_sync']:
                newState['last_sync'] = datetime.datetime.strptime(newState['last_sync'], guibits.DateTimeEncoder.date_fmt)
            else:
                newState['last_sync'] = None
        except Exception as e:
            Log.log('[uctd-model] Processing FAILED!', Log.VERB_ERROR)
            Log.logException('[uctd-model]', Log.VERB_ERROR)
            return

        self._state = newState

    def saveState(self):
        logfile = os.path.join(self._controller.logDir, 'probe.json')
        json.dump(self._state, open(logfile, 'w'), cls=guibits.DateTimeEncoder)
        Log.log('[uctd-model] Saving probe state to \"%s\"' % logfile, Log.VERB_WARN)


    def run(self):
        self.connect()
        self.getProbeStatus(connected=True)
        self.syncCasts(connected=True)
        self.setProbeTime(connected=True)

    def connect(self):
        # "Dummy" sync
        self.statusChanged.emit('Searching for probe...')
        Log.log('[uctd-model] Searching for probe', Log.VERB_DEBUG)
        self._probe.findAddress()
        if self._probe.getAddress() is None:
            Log.log('[uctd-model] Could not find probe', Log.VERB_WARN)
            raise Exception('Could not find probe')
        else:
            Log.log('[uctd-model] Got probe with address \"%s\"' % self._probe.getAddress(), Log.VERB_INFO)

        self.statusChanged.emit('Connecting to probe...')
        Log.log('[uctd-model] Connecting to probe', Log.VERB_DEBUG)
        self._probe.connect()

    def getProbeStatus(self, connected=False):
        if not connected:
            self.connect()

        self.statusChanged.emit('Getting status...')
        Log.log('[uctd-model] Getting probe status', Log.VERB_DEBUG)
        status = self._probe.getStatus()
        limits = self._config['limits']

        minVoltage = self._config['sync']['voltage_low']
        maxVoltage = self._config['sync']['voltage_high']
        voltageRange = maxVoltage - minVoltage
        self._state['probe_state']['voltage_value'] = ('%.2f V' % status['battery'])
        bat_pct = (100.0*float(status['battery'] - minVoltage) / float(voltageRange))
        self._state['probe_state']['voltage_pct'] = ('%.2f %%' % bat_pct)

        self._state['probe_flags']['voltage_pct'] = False
        if ('voltage_pct_low' in limits.keys() and bat_pct <= self._config['limits']['voltage_pct_low']):
            Log.log('[uctd-model] Probe voltage too low!', Log.VERB_WARN)
            self._state['probe_flags']['voltage_pct'] = True

        if ('voltage_pct_high' in limits.keys() and bat_pct >= self._config['limits']['voltage_pct_high']):
            Log.log('[uctd-model] Probe voltage too high!', Log.VERB_WARN)
            self._state['probe_flags']['voltage_pct'] = True

        self._state['probe_state']['data_value'] = ('%d' % status['samples_used'])
        data_pct = status['samples_used']*100.0 / (status['samples_used'] + status['samples_free'])
        self._state['probe_state']['data_pct'] = '%.2f %%' % data_pct
        self._state['probe_flags']['data_pct'] = False
        if ('data_pct_high' in limits.keys() and data_pct >= self._config['limits']['data_pct_high']):
            Log.log('[uctd-model] Not enough data left on probe!', Log.VERB_WARN)
            self._state['probe_flags']['data_pct'] = True


        self._state['last_sync'] = datetime.datetime.utcnow()
        self.saveState()
        # Send update to GUI
        self.probeStateUpdate.emit(self._state)

    def syncCasts(self, connected=False):
        if not connected:
            self.connect()

        self.statusChanged.emit('Getting cast list...')
        Log.log('[uctd-model] Getting cast list', Log.VERB_DEBUG)
        castList = self._probe.getCastList()

        #Dump all casts
        cruiseId = self.getCruiseId() # This queries a server do it ONCE!
        for cast in castList:
            
            #Figure out what filename we'd use
            datestr = cast['time'].strftime('%Y%m%d_%H%M%S')
            if cruiseId:
                fname = 'UCTD_%s_%05d_%s.asc' % (cruiseId,
                        cast['idnum'], datestr)
            else:
                fname = 'UCTD_UNKNOWN_%05d_%s.asc' % (cast['idnum'], datestr)
            outpath = os.path.join(self._controller.rawDir, fname)

            # Don't dump if file already copied
            if os.path.exists(outpath):
                continue

            self.statusChanged.emit('Dumping cast %d of %d...' % (cast['idnum'], len(castList)))
            Log.log('[uctd-model] Dumping cast %d of %d to \"%s\"' % (cast['idnum'], len(castList), fname), Log.VERB_ALERT)
            self._probe.exportCast(cast['idnum'], open(outpath, 'w'))

    def setProbeTime(self, connected=False):
        if not connected:
            self.connect()

        self.statusChanged.emit('Setting probe time...')
        Log.log('[uctd-model] Setting probe time', Log.VERB_DEBUG)
        self._probe.setTime()

    def wipeProbe(self, connected=False):
        if not connected:
            self.connect()

        self.statusChanged.emit('Wiping probe...')
        Log.log('[uctd-model] Wiping probe data', Log.VERB_ERROR)
        self._probe.wipeProbe()


    def sendProbeState(self):
        self.probeStateUpdate.emit(self._state)

    def updateCasts(self):

        Log.log('[uctd-model] Counting casts since last re-term', Log.VERB_DEBUG)

        # Figure out how many casts to add since our last re-term
        castCount = int(self._dataCfg['extraCasts'])

        # Figure out when the last-reterm was
        if isinstance(self._dataCfg['castResetTime'], datetime.datetime):
            castResetTime = self._dataCfg['castResetTime']
        else:
            castResetTime = datetime.datetime.strptime(self._dataCfg['castResetTime'], '%Y-%m-%dT%H:%M:%S')

        # Final number of casts is extra + # since last re-term
        for cast in self._controller._procModel._casts.getSortedCastList():
            if cast['valid'] and cast['time'] > castResetTime:
                castCount += 1

        # Great, we know how many casts we've done.  Update the state and save.
        self._state['probe_state']['cast_value'] = '%d' % castCount
        cast_pct = 100.0 * float(castCount) / float(self._config['sync']['max_casts'])
        self._state['probe_state']['cast_pct'] = '%.2f %%' % cast_pct

        limits = self._config['limits']
        self._state['probe_flags']['cast_pct'] = False
        if ('cast_pct_high' in limits.keys() and cast_pct >= self._config['limits']['cast_pct_high']):
            self._state['probe_flags']['cast_pct'] = True
            Log.log('[uctd-model] Too many casts since last re-term, reterminate spectra on probe!', Log.VERB_WARN)

        self.saveState()
        self.probeStateUpdate.emit(self._state)


class ProcessingModel(QtCore.QObject):
    statusChanged = QtCore.pyqtSignal('QString')
    fileListUpdate = QtCore.pyqtSignal(list)

    def __init__(self, config, controller, parent=None):
        super(ProcessingModel, self).__init__(parent)
        self._controller = controller
        self._dataCfg = config['layout']
        self._procCfg = self._controller._procCfg

        # Load old cast metadata, if available
        #self.statusChanged.emit('Loading previous casts...') # no point, nobody will be listening yet
        self._casts = Ctd.CastPool(self._controller.rawDir, self._controller.procDir, self._controller.logDir, self._procCfg)
        self._casts.loadCache()

        self._navserve = navserve.Client(config['navserve']['addr'], int(config['navserve']['port']))

    def sendCastTable(self):
        self.fileListUpdate.emit(self._casts.getSortedCastList())

    @QtCore.pyqtSlot(dict)
    def updateProcCfg(self, cfg):
        # TODO: Mutex-protect me!
        self._procCfg = cfg
        
    def run(self):

        # Actually process data
        self.statusChanged.emit('Processing Raw...')
        Log.log('[proc-model] Processing all casts', Log.VERB_INFO)
        #print 'Using  rawdir: \"%s\"' % self._rawDir
        #print 'Using procdir: \"%s\"' % self._procDir
        #print 'Using  logdir: \"%s\"' % self._logDir
        self._casts.updateDir(server=self._navserve)
        self._casts.writeCache()

        # We're done processing.  Send changes to the GUI
        Log.log('[proc-model] Finished cast processing, sending to GUI', Log.VERB_DEBUG)
        self.fileListUpdate.emit(self._casts.getSortedCastList())

    def markCastsSeen(self):
        self.statusChanged.emit('Marking casts seen...')
        Log.log('[proc-model] Marking all casts as not-new', Log.VERB_DEBUG)
        self._casts.markCastsSeen()
        self._casts.writeCache()

        # We're done processing.  Send changes to the GUI
        self.fileListUpdate.emit(self._casts.getSortedCastList())

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

class CastPlotCanvas(FigureCanvas):
    ''' This is a QWidget from Matplotlib '''
    def __init__(self, parent=None):
        # Bookeeping
        self._fig = Figure()
        super(CastPlotCanvas, self).__init__(self._fig)
        self.setParent(parent)

        # Prepare the underlying widget
        self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

    def plotCast(self, cast, title):
        self._cast = cast
        self._fig.clear()

        # Do the matplotlib stuff
        self._axes_temp = self._fig.add_subplot(131)
        self._axes_sal  = self._fig.add_subplot(132, sharey=self._axes_temp)
        self._axes_sv   = self._fig.add_subplot(133, sharey=self._axes_temp)

        self._axes_temp.plot(self._cast.temp, self._cast.depth)
        self._axes_sal .plot(self._cast.sal , self._cast.depth)
        self._axes_sv  .plot(self._cast.sv  , self._cast.depth)

        self._axes_temp.invert_yaxis()
        self._axes_sal .invert_yaxis()
        self._axes_sv  .invert_yaxis()

        self._axes_temp.grid(True)
        self._axes_sal .grid(True)
        self._axes_sv  .grid(True)

        self._axes_temp.locator_params(axis='y', nbins=8)
        self._axes_temp.locator_params(axis='x', nbins=6)
        self._axes_sal .locator_params(axis='x', nbins=4)
        self._axes_sv  .locator_params(axis='x', nbins=4)
        [x.set_visible(False) for x in self._axes_sal.get_yaxis().get_ticklabels()]
        [x.set_visible(False) for x in self._axes_sv .get_yaxis().get_ticklabels()]

        self._axes_temp.set_title('Temperature')
        self._axes_temp.set_xlabel('deg C')
        self._axes_temp.set_ylabel('Depth [m]')

        self._axes_sal .set_title('Salinity')
        self._axes_sal .set_xlabel('psu')

        self._axes_sv  .set_title('Sound Velocity')
        self._axes_sv  .set_xlabel('m/s')

        self._fig.suptitle(title, fontweight='bold')
        self.draw()

class CastPlotGui(QtGui.QWidget):

    windowClosed = QtCore.pyqtSignal()

    def __init__(self, parent=None, icon=None, runFullscreen=False):
        # bookkeeping
        super(CastPlotGui, self).__init__(parent)
        self.setAttribute(QtCore.Qt.WA_QuitOnClose)

        # Get our widgets ready
        self.plot = CastPlotCanvas(self)

        self.quitButton = QtGui.QPushButton("Done", self)
        self.quitButton.setFixedHeight(50)
        self.quitButtonFont = QtGui.QFont()
        self.quitButtonFont.setPointSize(16)
        self.quitButtonFont.setBold(True)
        self.quitButton.setFont(self.quitButtonFont)

        # Setup the layout
        self.layout = QtGui.QVBoxLayout(self)
        self.layout.addWidget(self.plot)
        self.layout.addWidget(self.quitButton)
        self.setLayout(self.layout)

        self.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        pos = self.pos()
        # If on a desktop, hard-code to the same size as a raspberry pi screen
        self.setGeometry(pos.x(), pos.y(), 800, 480)
        self.setMaximumSize(800, 480)

        # Window stuff
        if icon:
            self.setWindowIcon(icon)

        # Wire up events
        self.quitButton.clicked.connect(self.close)

    def plotCast(self, cast):
        if hasattr(cast, 'filename'):
            title = os.path.basename(cast.filename)
        else:
            title = cast.castTime.isoformat()

        self.plot.plotCast(cast, title)
        self.setWindowTitle(title)


    def closeEvent(self, event):
        self.windowClosed.emit()
        event.accept()

class UctdGui(QtGui.QMainWindow):

    syncStarted = QtCore.pyqtSignal()

    def __init__(self, config, controller, parent=None, runFullscreen=False):
        super(UctdGui, self).__init__(parent)
        self._controller = controller
        self._show_invalid_casts = bool(config['gui']['show_invalid_casts'])
    
        filepath = os.path.realpath(os.path.dirname(__file__))
        sharedir = os.path.join(filepath, os.path.pardir, 'share')
        uic.loadUi(os.path.join(filepath, 'uctd.ui'), self)
        self._castGui = None

        logoPixmap = QtGui.QPixmap(os.path.join(sharedir, 'OET_Logo_BUG_COLOR_crop.png'))
        self._icon = QtGui.QIcon(os.path.join(sharedir, 'OET_compass_small.png'))
        self.logo.setPixmap(logoPixmap)
        self.setWindowIcon(self._icon)
        self._newCastFont = QtGui.QFont()
        self._newCastFont.setBold(True)

        self._fullscreen = runFullscreen
        if self._fullscreen:
            self.showFullScreen()
        else:
            self.show()

        self.castTable.setColumnCount(2)
        self.castTable.setHorizontalHeaderLabels(['Depth', 'Processed Filename'])
        self.castTable.setColumnWidth(0, 50)
        self.castTable.horizontalHeader().setResizeMode(1, QtGui.QHeaderView.Stretch)
        # disable editing
        self.castTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.castTable.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.castTable.clicked.connect(self.rowClicked)

        # Wire the menu items
        self.actionQuit.triggered.connect(self.handleQuit)

        self.actionGet_Probe_Status.triggered.connect(self._controller.getProbeStatus)
        self.actionSync_Casts.triggered.connect(self._controller.syncCasts)
        self.actionSync_Time.triggered.connect(self._controller.setProbeTime)
        self.actionWipe_Probe.triggered.connect(self.wipeProbe)

        self.actionRun.triggered.connect(self._controller.runProc)
        self.actionMark_all_casts_seen.triggered.connect(self._controller.markAllCastsSeen)
        self.actionRefresh_Count.triggered.connect(self._controller.updateCastCount)

        # And the big button!
        self.syncButton.released.connect(self.runSync)

    def rowClicked(self, idx):
        self.lockOutUser()
        QtGui.qApp.processEvents() # Re-draw to actually lock out users

        filename = self.castTable.item(idx.row(), 1).text()
        Log.log('[gui] Displaying cast %d from file \"%s\"' % (idx.row(), filename), Log.VERB_INFO)

        # Make sure this isn't garbage collected
        if self._castGui is None:
            self._castGui = CastPlotGui(icon=self._icon)
            self._castGui.windowClosed.connect(self.unlockUser)

        # Show the widget so the user knows something is happening
        if self._fullscreen:
            self._castGui.showFullScreen()
        else:
            self._castGui.show()

        # Actually load data
        cast = Ctd.Cast()
        cast.loadCnv(os.path.join(self._controller.procDir, str(filename)))
        self._castGui.plotCast(cast)

    @QtCore.pyqtSlot()
    def handleQuit(self):
        self.close()

    def closeEvent(self, event):
        reply = QtGui.QMessageBox.question(self, 'Confirm',
                'Are you sure you want to quit?',
                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            event.accept()
            if self._castGui is not None:
                self._castGui.close()
        else:
            event.ignore()
        
    def setStatusBarMsg(self, msg):
        self.statusBar().showMessage(msg)
        
    @QtCore.pyqtSlot(str)
    def handleStatus(self, newStatus):
        self.status.setStyleSheet('color: blue')
        self.status.setText(newStatus)

    @QtCore.pyqtSlot(dict)
    def handleProbeUpdate(self, probeValues):
        Log.log('[gui] Updating probe state display', Log.VERB_DEBUG)
        probeState = probeValues['probe_state']
        probeErrors = probeValues['probe_flags']
        for (key, value) in probeState.items():
            self.__dict__[key].setText(value)
            if key in probeErrors.keys() and probeErrors[key]:
                self.__dict__[key].setStyleSheet('color: rgb(255,255,255); background-color: rgb(255, 0, 0)')
            else:
                self.__dict__[key].setStyleSheet('')

        syncTime = probeValues['last_sync']
        if syncTime:
            self.time_last_sync.setText('Last Sync: ' + syncTime.strftime(TIME_FORMAT) + ' UTC')
        else:
            self.time_last_sync.setText('Last Sync: UNKNOWN')

    def wipeProbe(self):
        reply = QtGui.QMessageBox.question(self, 'Sync\'d?',
                'Was the probe sync\'d since the last cast?',
                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.No:
            return

        reply = QtGui.QMessageBox.question(self, 'Are you sure?',
                'This will wipe all data off the probe.  If you don\'t know what this means, you don\'t want to do it!',
                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.No:
            return

        Log.log('[gui] Asked to wipe probe', Log.VERB_ALERT)
        self._controller.wipeProbe()


    @QtCore.pyqtSlot(dict)
    def handleCastTableUpdate(self, castTable):
        Log.log('[gui] Updating cast table', Log.VERB_DEBUG)
        lastCast = None
        row = 0
        self.castTable.setRowCount(len(castTable))
        for idx in range(0, len(castTable)):
            if not(self._show_invalid_casts or castTable[idx]['valid']):
                continue

            self.castTable.setItem(row, 0, QtGui.QTableWidgetItem(castTable[idx]['depth']))
            dispName = castTable[idx]['filename']
            if not dispName:
                dispName = 'Not Converted'
            self.castTable.setItem(row, 1, QtGui.QTableWidgetItem(dispName))

            if castTable[idx]['new']:
                self.castTable.item(row, 0).setFont(self._newCastFont)
                self.castTable.item(row, 1).setFont(self._newCastFont)

            if castTable[idx]['valid'] and (lastCast is None or castTable[idx]['time'] > lastCast):
                lastCast = castTable[idx]['time']

            row += 1

        self.castTable.setRowCount(row)
        if lastCast is not None:
            self.time_last_cast.setText('Last Cast: ' + lastCast.strftime(TIME_FORMAT) + ' UTC')

        # ALWAYS scroll to the bottom
        QtGui.qApp.processEvents() # First, redraw so the scroll bar gets set to the correct max
        self.castTable.verticalScrollBar().setValue(self.castTable.verticalScrollBar().maximum())


    def runSync(self):
        self.syncStarted.emit()

    def lockOutUser(self):
        for widget in self.findChildren(QtGui.QLabel):
            if widget != self.status:
                widget.setEnabled(False)
        self.syncButton.setEnabled(False)
        self.menubar.setEnabled(False)
        self.castTable.setEnabled(False)

    def unlockUser(self):
        for widget in self.findChildren(QtGui.QWidget):
            widget.setEnabled(True)

    def onTick(self):
        now = datetime.datetime.utcnow()
        self.time_now.setText('Currently: ' + now.strftime(TIME_FORMAT) + ' UTC')


class UctdControl(QtCore.QObject):
    statusChanged = QtCore.pyqtSignal('QString')

    def __init__(self, args, parent=None):
        super(UctdControl, self).__init__(parent)

        # Set our initial state
        config = yaml.load(open(args.config, 'r'))
        self.cruiseId = config['layout']['cruise']
        self.lastSyncTime = datetime.datetime.utcnow()
        self._procCfg = config['proc']
        self._dataCfg = config['layout']

        self.updateDirs()

        # Setup logging
        Log.setPath(os.path.join(self.logDir, 'uctd_acq.log'))
        Log.log('[control] Starting UCTD acq/proc GUI', Log.VERB_ALWAYS)

        # Initialize various models and GUI elements
        self._uctdModel = UctdModel(config, self)
        self._procModel = ProcessingModel(config, self)
        self._view      = UctdGui(config, self, runFullscreen=args.fullscreen)
        self._worker = Worker(self)

        # Setup the worker thread to process our models
        # (Model View Controller sense, not scientific modelling)
        self._worker.addProcess('uctd', self._uctdModel)
        self._worker.addProcess('proc', self._procModel)

        # Wire the signals
        self._uctdModel.probeStateUpdate.connect(self._view.handleProbeUpdate, type=QtCore.Qt.QueuedConnection)
        self._uctdModel.probeStateUpdate.connect(self.handleProbeUpdate, type=QtCore.Qt.QueuedConnection)
        self._uctdModel.statusChanged.connect(self._view.handleStatus, type=QtCore.Qt.QueuedConnection)
        self._procModel.fileListUpdate.connect(self._view.handleCastTableUpdate, type=QtCore.Qt.QueuedConnection)
        self.statusChanged.connect(self._view.handleStatus)

        self._worker.processingDone.connect(self.doneRunning, QtCore.Qt.QueuedConnection)
        self._view.syncStarted.connect(self.runSync)

        # Start the worker thread:
        self._worker.start()

        # Fire off an initial status
        self.readyMsg = 'Insert Plug in Probe\nand Push \"Synchronize\"'
        self.statusChanged.emit(self.readyMsg)
        self._view.onTick()

        # Start our clock updater
        self.ticker = QtCore.QTimer()
        self.ticker.timeout.connect(self._view.onTick)
        self.ticker.start(1000)

        # Ask the processing model to fire off an initial cast update
        Log.log('[control] Sending initial cast table and probe status', Log.VERB_DEBUG)
        self._worker.runProcess('proc', 'sendCastTable', None)
        self._worker.runProcess('uctd', 'sendProbeState', None)

    @QtCore.pyqtSlot()
    def runSync(self):
        self._view.lockOutUser()
        self._worker.runProcess(None, None, None)

    @QtCore.pyqtSlot()
    def runProc(self):
        self._view.lockOutUser()
        self._worker.runProcess('proc', 'run', None)
        self._worker.runProcess('uctd', 'updateCasts', None)

    @QtCore.pyqtSlot()
    def updateCastCount(self):
        self._view.lockOutUser()
        self._worker.runProcess('uctd', 'updateCasts', None)

    @QtCore.pyqtSlot()
    def markAllCastsSeen(self):
        self._view.lockOutUser()
        self._worker.runProcess('proc', 'markCastsSeen', None)

    @QtCore.pyqtSlot()
    def getProbeStatus(self):
        self._view.lockOutUser()
        self._worker.runProcess('uctd', 'getProbeStatus', None)

    @QtCore.pyqtSlot()
    def syncCasts(self):
        self._view.lockOutUser()
        self._worker.runProcess('uctd', 'syncCasts', None)

    @QtCore.pyqtSlot()
    def setProbeTime(self):
        self._view.lockOutUser()
        self._worker.runProcess('uctd', 'setProbeTime', None)

    @QtCore.pyqtSlot()
    def wipeProbe(self):
        self._view.lockOutUser()
        self._worker.runProcess('uctd', 'wipeProbe', None)

    @QtCore.pyqtSlot(dict)
    def handleProbeUpdate(self, probeValues):
        self._probeState = probeValues

    @QtCore.pyqtSlot(bool, str)
    def doneRunning(self, success, msg):
        #print ''.join(['-']*80)
        if success:
            #print 'Processing completed successfully!'
            self._view.setStatusBarMsg('Last Command Succeeded!')
            Log.log('[control] Successfully finished last command', Log.VERB_ALERT)
        else:
            #print 'Processing FAILED!'
            self._view.setStatusBarMsg(msg)
            Log.log('[control] ERROR during last command!', Log.VERB_ERROR)
            Log.log('[control] msg: %s' % msg, Log.VERB_ERROR)
        self._view.unlockUser()
        self.lastSyncTime = datetime.datetime.utcnow()
        self.statusChanged.emit(self.readyMsg)
        Log.log('[control] Finished processing step, setting ready', Log.VERB_INFO)

    def updateDirs(self):
        self.rawDir = os.path.join(self._dataCfg['base'],
                self._dataCfg['rawDir'])

        self.procDir = os.path.join(self._dataCfg['base'],
                self._dataCfg['procDir'])

        self.logDir = os.path.join(self._dataCfg['base'],
                self._dataCfg['logDir'])

        if not os.path.isdir(self.rawDir):
            if os.path.exists(self.rawDir):
                raise ValueError("Invalid raw directory \"%s\"" % self.rawDir)
            os.makedirs(self.rawDir)

        if not os.path.isdir(self.procDir):
            if os.path.exists(self.procDir):
                raise ValueError("Invalid processed directory \"%s\"" % self.procDir)
            os.makedirs(self.procDir)

        if not os.path.isdir(self.logDir):
            if os.path.exists(self.logDir):
                raise ValueError("Invalid log directory \"%s\"" % self.logDir)
            os.makedirs(self.logDir)



if __name__ == '__main__':

    # Create the application
    app = QtGui.QApplication(sys.argv)

    # Re-use our custom QtArgumentParser class to not confuse QT arguments
    parser = guibits.QtArgumentParser(description='UCTD Acquisition / Conversion GUI')
    parser.add_argument('config', help='Primary YAML config file')
    parser.add_argument('-f', '--fullscreen', action='store_true', help='Run in fullscreen mode')
    parser.add_argument('-v', '--verbosity', default='alert', help='Verbosity level for printing to logs and the screen.  Default=alert, options are [ error | warn | alert | info | debug ]')
    args = parser.parse_app(app)
    Log.parseVerbosity(args.verbosity)
    controller = UctdControl(args)

    signal.signal(signal.SIGINT, guibits.quit_clean)
    sys.exit(app.exec_())
