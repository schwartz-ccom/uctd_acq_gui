#!/usr/bin/python

#The MIT License (MIT)
#
#Copyright (c) 2016, Ocean Exploration Trust
#Copyright (c) 2016, J. Ian S. Vaughn
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import NGcore.NGtime
import datetime

import urllib
try:
    import urllib.request as urllib2
except ImportError:
    import urllib2
import json

class NavServeException(Exception):
    pass

class Client(object):
    def __init__(self, addr, port=8080):
        self._addr = addr
        self._port = port
        self._base_url = 'http://%s:%d/' % (self._addr, self._port)

    def getNavByTime(self, vehicle, time):
        data = {'vehicle': vehicle}
        if isinstance(time, datetime.datetime):
            data['time'] = time.isoformat() + 'Z'
        else:
            data['time'] = time

        url_values = urllib.urlencode(data)
        url = self._base_url + 'navbytime/?' + url_values

        return  self._get(url)

    def getCruiseId(self):
        url = self._base_url + 'cruiseId'
        obj = self._get(url)
        return obj['cruiseId']

    def _get(self, url):
        #print url
        response = urllib2.urlopen(url)
        obj = json.loads(response.read())
        if obj['ok']:
            return obj

        msg = obj['exception']
        msg_fmt = '\n'.join(['SERVER: %s' % line for line in msg.split('\n')])

        raise NavServeException(msg_fmt)



