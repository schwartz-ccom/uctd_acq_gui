#!/usr/bin/python

#The MIT License (MIT)
# 
#Copyright (c) 2016, Ocean Exploration Trust
#Copyright (c) 2016, J. Ian S. Vaughn
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import numpy as np
import scipy.interpolate
import matplotlib.pyplot as plt
import unittest
import os, datetime
import glob, json
import traceback

# Internal dependencies
from NGcore.pyguibits import DateTimeEncoder
import NGcore.QtLogger as Log
import NGcore.NGnmea


class Cast(object):
    def __init__(self):
        self.lat = None
        self.lon = None
        self.startT = None
        self.depth = None # Depth, m
        self.sal = None # Salinity, PSU
        self.temp = None # Temperature, ITS-90 deg C
        self.press = None # Pressure, decibars
        self.cond = None # conductivity, as reported by U-CTD
        self.sv = None # m/s
        self.scanNum = None
        self.bad_cast = True

    def parseFilename(self, filename):
        # Set the cast time and number from the filename
        parts = os.path.basename(filename)[:-4].split('_')
        if len(parts) < 5:
            raise ValueError("Bad U-CTD filename!: " + filename)
        self.castNum = int(parts[2])
        self.castTime = datetime.datetime.strptime(parts[3] + ' ' + parts[4], '%Y%m%d %H%M%S')

    def loadUCTD(self, filename):

        Log.log('[ctd] Loading CTD raw file \"%s\"' % filename, Log.VERB_INFO)
        self.parseFilename(filename)
        # Clear the processed fields
        self.depth = None
        self.sal = None
        self.sv = None

        # Prepare the measured fields
        scanNum = []
        cond = []
        temp = []
        press = []

        for line in open(filename, 'r'):
            if line[0] == '*':
                continue

            parts = line.split()
            if len(parts) < 4:
                continue
            scanNum.append(int(parts[0]))
            cond.append(float(parts[1]))
            temp.append(float(parts[2]))
            press.append(float(parts[3]))

        self.scanNum = np.array(scanNum)
        self.cond = np.array(cond)
        self.temp = np.array(temp)
        self.press = np.array(press)

        self.bad_cast = False # well, hopefully

    def invalid(self):
        return self.bad_cast

    def autoCrop(self, START_THRESH=1.0):
        if self.press is None or len(self.press) < 1:
            Log.log('[ctd] Cast failed minimum number of samples', Log.VERB_ALERT)
            self.bad_cast = True
            return

        if np.max(self.press) < START_THRESH:
            Log.log('[ctd] Cast failed max depth threshold, with %.2f < %.2f dbar' % (np.max(self.press), START_THRESH), Log.VERB_ALERT)
            self.bad_cast = True
            return
        startIdx = np.argmax(self.press > START_THRESH)
        endIdx = np.argmax(self.press)

        self.scanNum = self.scanNum[startIdx:endIdx]
        self.cond    = self.cond   [startIdx:endIdx]
        self.temp    = self.temp   [startIdx:endIdx]
        self.press   = self.press  [startIdx:endIdx]

    def plotRaw(self):
        ax1 = plt.subplot(311)
        ax1.plot(self.scanNum, self.press)
        ax1.set_ylabel('Pressure [dbar]')

        ax2 = plt.subplot(312)
        ax2.plot(self.scanNum, self.cond)
        ax2.set_ylabel('Conductivity')

        ax3 = plt.subplot(313)
        ax3.plot(self.scanNum, self.temp)
        ax3.set_ylabel('Temperature [deg C]')
        ax3.set_xlabel('Scan No.')

    def plotProc(self):
        ax1 = plt.subplot(131)
        ax1.plot(self.sv, self.depth)
        ax1.invert_yaxis()
        ax1.set_xlabel('Sound Velocity [m/s]')
        ax1.set_ylabel('Depth [m]')
        plt.grid()

        ax2 = plt.subplot(132)
        ax2.plot(self.temp, self.depth)
        ax2.invert_yaxis()
        ax2.set_xlabel('Temperature [deg C]')
        plt.grid()

        ax3 = plt.subplot(133)
        ax3.plot(self.sal, self.depth)
        ax3.invert_yaxis()
        ax3.set_xlabel('Salinity [psu]')
        plt.grid()

    def calcFields(self, lat):
        self.calc_sal()
        self.calc_depth(lat)
        self.calc_sv()

    def calc_sal(self, useT68=False):
        # (probably) do a temperature conversion
        if useT68:
            t68 = self.temp
        else:
            t68 = 1.00024*self.temp

        R = self.cond / 4.2914

        # Compute rt
        c0 = 0.6766097
        c1 = 2.00564e-2
        c2= 1.104259e-4
        c3 = -6.9698e-7
        c4 = 1.0031e-9

        rt = c0 + c1*t68 + c2*(t68**2) + c3*(t68**3) + c4*(t68**4)

        # Compute Rp
        e1 = 2.070e-5
        e2 = -6.370e-10
        e3 = 3.989e-15
        d1 = 3.426e-2
        d2 = 4.464e-4
        d3 = 4.215e-1
        d4 = -3.107e-3

        Rp = 1 + (self.press * (e1 + e2*self.press + e3*(self.press**2))  \
                / (1 + d1*t68 + d2*(t68**2) + (d3+d4*t68)*R))

        # Next, Rt:
        Rt = R / (Rp * rt)

        # Finally, convert to salinity:
        a0 =  0.0080
        a1 = -0.1692
        a2 = 25.3851
        a3 = 14.0941
        a4 = -7.0261
        a5 =  2.7081
        b0 =  0.0005
        b1 = -0.0056
        b2 = -0.0066
        b3 = -0.0375
        b4 =  0.0636
        b5 = -0.0144
        k =   0.0162

        sqrtRt = np.sqrt(Rt)
        self.sal = a0 + a1*sqrtRt + a2*Rt + a3*(sqrtRt**3) + a4*(Rt**2) \
            + a5*(sqrtRt**5) + (t68-15)/(1+k*(t68-15))* \
            (b0 + b1*sqrtRt + b2*Rt + b3*(sqrtRt**3) \
            + b4*(Rt**2) + b5*(sqrtRt**5))

    def calc_depth(self, lat):
        # Uses the UNESCO properties of seawater stuff

        # Compute latitude-dependent gravity
        g = 9.780318 * (1.0 + 5.2788e-3*np.sin(np.radians(lat))**2 \
                + 2.36e-5*np.sin(np.radians(lat))**4)

        # Convert pressure to depth
        c1 =  9.72659
        c2 = -2.2512e-5
        c3 =  2.279e-10
        c4 = -1.82e-15
        r  =  2.184e-6

        self.depth = (c1*self.press + c2*self.press**2 + c3*self.press**3 \
                + c4*self.press**4)/(g + 0.5 * r * self.press);
        # z is actually z + gravity anomaly, but we ignore the anomaly b/c its
        # usually pretty tiny

    def calc_sv(self):
        # Uses the Del Grosso equations (or rather, % Uses Wong & Zhu, 1994
        # (adapter for ITS-90 temperature)

        c000 = 1402.392
        ct1 =  0.5012285e1
        ct2 = -0.551184e-1
        ct3 =  0.221649e-3
        cs1 =  0.1329530e1
        cs2 =  0.1288598e-3
        cp1 =  0.1560592
        cp2 =  0.2449993e-4
        cp3 = -0.883959e-8
        cst = -0.1275936e-1
        ctp =  0.6353509e-2
        ct2p2 = 0.2656174e-7
        ctp2 = -0.1593895e-5
        ctp3 = 0.5222483e-9
        ct3p = -0.4383615e-6
        cs2p2 = -0.1616745e-8
        ct2s = 0.9688441e-4
        cs2tp = 0.4857614e-5
        ctsp = -0.3406824e-3

        #Convert from decibars to kg/cm^2 (how is this a unit of pressure???)
        # Because it's a set force over a set area
        p = self.press*0.1019716

        dCT = ct1*self.temp + ct2*(self.temp**2) + ct3*(self.temp**3)
        dCS = cs1*self.sal + cs2*(self.sal**2)
        dCP = cp1*p + cp2*(p**2) + cp3*(p**3)
        dCSTP = ctp*self.temp*p + ct3p*(self.temp**3)*p \
                + ctp2*self.temp*(p**2) + ct2p2*(self.temp**2)*(p**2) \
                + ctp3*self.temp*(self.press**3) + cst*self.sal*self.temp \
                + ct2s*self.sal*(self.temp**2) + ctsp*self.sal*self.temp*p \
                + cs2tp*(self.sal**2)*self.temp*p + cs2p2*(self.sal**2)*(p**2)

        self.sv = c000 + dCT + dCS + dCP + dCSTP



    def shiftTemp(self, shift):
        ''' Shift temperature by shfit sample to account for lag and reduce salinity spiking '''
        fx = scipy.interpolate.interp1d(range(0,self.temp.size), self.temp)
        idx = int(np.ceil(np.abs(shift)))
        if shift > 0:
            # Interpolate
            self.temp = fx(shift+np.array(range(0,self.temp.size - idx)))
            # Cut down to the correct size
            self.scanNum = self.scanNum[:-idx]
            self.cond  = self.cond[:-idx]
            self.press = self.press[:-idx]
            if self.depth is not None:
                self.depth = self.depth[:-idx]
            if self.sal is not None:
                self.sal   = self.sal[:-idx]
            if self.sv is not None:
                self.sv    = self.sv[:-idx]
        else:
            # Interpolate
            self.temp = fx(shift+np.array(range(idx,self.temp.size)))
            # Cut down to the correct size
            self.scanNum = self.scanNum[idx:]
            self.cond  = self.cond[idx:]
            self.press = self.press[idx:]
            if self.depth is not None:
                self.depth = self.depth[idx:]
            if self.sal is not None:
                self.sal   = self.sal[idx:]
            if self.sv is not None:
                self.sv    = self.sv[idx:]

    def exportCnv(self, filename):
        timeStr = self.castTime.strftime('%b %d %Y %H:%M:%S')
        Log.log('[ctd] Exporting cast to CNV file \"%s\"' % filename, Log.VERB_INFO)
        outfd = open(filename, 'w')
        if self.lat:
            latStr = '%02d %07.4f %s' % NGcore.NGnmea.buildLL_bits(self.lat,True)
            outfd.write('* NMEA Latitude = %s\n' % latStr)
        if self.lon:
            lonStr = '%03d %07.4f %s' % NGcore.NGnmea.buildLL_bits(self.lon,False)
            outfd.write('* NMEA Longitude = %s\n' % lonStr)
        outfd.write('* NMEA UTC (Time) = %s\n' % timeStr)
        outfd.write('# nquan = 5\n')
        outfd.write('# nvalues = %d\n' % self.scanNum.size)
        outfd.write('# units = specified\n')
        outfd.write('# name 0 = depSM: Depth [salt water, m]\n')
        outfd.write('# name 1 = t090C: Temperature [ITS-90, deg C]\n')
        outfd.write('# name 2 = sal00: Salinity, Practical [PSU]\n')
        outfd.write('# name 3 = svCM: Sound Velocity [Del Grosso, m/s]\n')
        outfd.write('# name 4 = prDM: Pressure [db]\n')
        outfd.write('# name 5 = c0S/m: conductivity [S/m]\n')
        outfd.write('# name 6 = flag:  0.000e+00\n')
        outfd.write('# file_type = ascii\n')
        outfd.write('*END*\n')

        for i in range(0, self.scanNum.size):
            outfd.write(' %10.3f %10.4f %10.4f %10.2f %10.3f %10.5f 0.000e+00\n' % \
                    (self.depth[i], self.temp[i], self.sal[i], self.sv[i], self.press[i], self.cond[i]))
        outfd.close()

    def loadCnv(self, filename):
        Log.log('[ctd] Loading processed cast CNV file \"%s\"' % filename, Log.VERB_INFO)
        self.parseFilename(filename)

       # Clear the processed fields
        self.depth = None
        self.sal = None
        self.sv = None
        self.lat = None
        self.lon = None
        self.filename = filename

        # Prepare the measured fields
        scanNum = []
        dep = []
        temp = []
        sal = []
        sv = []
        press = []
        cond = []
        count = 0

        for line in open(filename, 'r'):
            # Skip comments
            if line[0] == '#':
                continue

            # Parse header stuff
            if line.startswith('* NMEA Latitude = '):
                payload = line.split('=')[1].strip()
                self.lat = NGcore.NGnmea.parseLL(payload[:-1], payload[-1])
                Log.log('[ctd] Found latitude=%.6f in CNV file' % self.lat, Log.VERB_DEBUG)
                continue
            if line.startswith('* NMEA Longitude = '):
                payload = line.split('=')[1].strip()
                self.lon = NGcore.NGnmea.parseLL(payload[:-1], payload[-1])
                Log.log('[ctd] Found longitude=%.6f in CNV file' % self.lon, Log.VERB_DEBUG)
                continue
            if line.startswith('* NMEA UTC (Time) = '):
                payload = line.split('=')[1].strip()
                self.castTime = datetime.datetime.strptime(payload, '%b %d %Y %H:%M:%S')
                Log.log('[ctd] Found time=%s in CNV file' % self.castTime.isoformat(), Log.VERB_DEBUG)
                continue

            # Ok, no other header data worth parsing
            if line[0] == '*':
                continue


            parts = line.split()
            if len(parts) < 7:
                continue
            scanNum.append(count)
            count += 1
            dep.append  ( float(parts[0]) )
            temp.append ( float(parts[1]) )
            sal.append  ( float(parts[2]) )
            sv.append   ( float(parts[3]) )
            press.append( float(parts[4]) )
            cond.append ( float(parts[5]) )

        if len(dep) < 2:
            self.bad_cast = True
            return

        self.scanNum = np.array(scanNum)
        self.depth = np.array(dep)
        self.temp = np.array(temp)
        self.sal = np.array(sal)
        self.sv = np.array(sv)
        self.press = np.array(press)
        self.cond = np.array(cond)

        self.bad_cast = np.max(self.depth) < 1.5

        Log.log('[ctd] Got %d samples from CNV, badcast=%s' % (len(self.depth), str(self.bad_cast)), Log.VERB_DEBUG)

    def getMaxDepth(self):
        if self.depth is None:
            return None
        return np.max(self.depth)


class CastPool(object):
    def __init__(self, rawDir, procDir, logDir, procCfg):
        self._raw = rawDir
        self._proc = procDir
        self._log = logDir
        self._proc_cfg = procCfg
        self.castTable = {}

    def loadCache(self):
        logfile = os.path.join(self._log, 'casts.json')
        Log.log('[ctd-pool] Looking for cast index in \"%s\"' % logfile, Log.VERB_DEBUG)
        if not os.path.isfile(logfile):
            Log.log('[ctd-pool] No cast index found at \"%s\"' % logfile, Log.VERB_WARN)
            return
        try:
            self.castTable = json.load(open(logfile, 'r'))
            for cast in self.castTable.values():
                if isinstance(cast['time'], basestring):
                    cast['time'] = datetime.datetime.strptime(cast['time'], DateTimeEncoder.date_fmt)

        except Exception as e:
            Log.log('[ctd-pool] Could not load cast index file \"%s\"' % logfile, Log.VERB_ERROR)
            Log.logException('[ctd-pool]', Log.VERB_ERROR)

    def writeCache(self):
        logfile = os.path.join(self._log, 'casts.json')
        Log.log('[ctd-pool] Saving cast index to \"%s\"' % logfile, Log.VERB_DEBUG)
        json.dump(self.castTable, open(logfile, 'w'), cls=DateTimeEncoder)

    def updateDir(self, server=None):

        # Mark all existing casts as not new
        Log.log('[ctd-pool] Looking for new casts in \"%s\"' % self._raw, Log.VERB_INFO)

        # Look for new raw files
        for fname in sorted(glob.glob(os.path.join(self._raw, '*.asc'))):

            # (Possibly) skip processing a file that was already done
            #print ''.join(['-']*80)
            #print 'Checking file \"%s\"' % fname
            base = os.path.basename(fname)
            if base in self.castTable.keys():
                if not self.castTable[base]['valid']:
                    # Cast has been processed and found to be invalid, skip
                    self.castTable[base]['new'] = False
                    Log.log('[ctd-pool] Raw file \"%s\" is marked as \"invalid\" in the index' % base, Log.VERB_DEBUG)
                    continue
                cnvfile = os.path.join(self._proc, self.castTable[base]['filename'])
                if os.path.isfile(cnvfile):
                    # File has been processed, skip it
                    self.castTable[base]['new'] = False
                    Log.log('[ctd-pool] Found existing processed file for raw file \"%s\"' % base, Log.VERB_DEBUG)
                    continue

            # We have no record of this file, and/or it doesn't exist.  So process it...
            try:
                self.castTable[base] = self.processCast(fname, server=server)
                self.castTable[base]['new'] = True
            except Exception as e:
                Log.log('[ctd-pool] Could not process cast from file \"%s\"' % fname, Log.VERB_ERROR)
                Log.logException('[ctd-pool]', Log.VERB_ERROR)

    def processCast(self, filename, server=None):
        base = os.path.basename(filename)
        Log.log('[ctd-pool] Processing cast from file \"%s\"' % base, Log.VERB_DEBUG)
        #print 'Processing cast \"%s\"' % base

        cast = Cast()
        cast.loadUCTD(filename)
        cast.autoCrop(START_THRESH=float(self._proc_cfg['start_thresh_m']))
        # Start the result
        ret = {'filename': None, 'valid': False, 'depth': 'N/A',
                'time': cast.castTime, 'num': cast.castNum, 'raw_file': base}

        #ret['new'] = not os.path.isfile(os.path.join(self._proc, base))
        if cast.invalid():
            print ( 'Cast \"%s\" has no valid data' % filename )
            # If we want invalid casts to appear...
            return ret

        lat = None
        lon = None
        lat_press = None
        if server is not None:
            try:
                obj = server.getNavByTime("nautilus", cast.castTime.isoformat() + 'Z')
                cast.lat = float(obj['lat'])
                cast.lon = float(obj['lon'])
                lat_press = cast.lat
                Log.log('[ctd-pool] Using lat/lon from server', Log.VERB_INFO)
            except:
                Log.log('[ctd-pool] Error getting position from server', Log.VERB_ERROR)
                Log.logException('[ctd]', Log.VERB_ERROR)
        else:
            Log.log('[ctd-pool] No nav server given.  THIS IS NOT RECOMMENDED!', Log.VERB_ERROR)


        if lat_press is None and 'lat_deg' in self._proc_cfg.keys():
            Log.log('[ctd-pool] Using crude latitude from config file for presssure -> depth ONLY', Log.VERB_ALERT)
            lat_press = float(self._proc_cfg['lat_deg'])

        if lat_press is None:
            Log.log('[ctd-pool] NO LATITUDE! Assuming 15 degrees for presssure -> depth ONLY', Log.VERB_WARN)
            lat_press = 15.0

        cast.shiftTemp(float(self._proc_cfg['temp_shift_s']))
        cast.calcFields(lat_press)

        # Get depth
        dep = cast.getMaxDepth()
        if dep:
            ret['depth'] = ('%.0f' % dep)
            ret['valid'] = True

        # Finish bookeeping for the UI
        procName = base[:-3] + 'cnv'
        ret['filename'] = procName

        # Export the cast to disk
        cast.exportCnv(os.path.join(self._proc, procName))

        Log.log('[ctd-pool] Finished processing cast from file \"%s\"' % base, Log.VERB_DEBUG)

        return ret

    def getSortedCastList(self):
        # Sort alphabetically by raw filename
        return sorted(self.castTable.values(), key=lambda o: o['raw_file'])

    def markCastsSeen(self):
        Log.log('[ctd-pool] Marking all casts as seen', Log.VERB_DEBUG)
        for cast in self.castTable.values():
            cast['new'] = False

class TestCast(unittest.TestCase):

    def test_cond_to_sal(self):
        underTest = Cast()
        underTest.scanNum = np.array([1,2,3])
        underTest.cond = np.array([1.0, 1.2, 0.65])*4.2914
        underTest.temp = np.array([15, 20, 5])
        underTest.press = np.array([0, 2000, 1500])
        # Test cases from the UNESCO document
        underTest.calc_sal(useT68=True)
        expected = [35.0, 37.245628, 27.995347]
        for i in range(0,3):
            self.assertAlmostEqual(expected[i], underTest.sal[i], 6)

    def test_press_to_z(self):
        # Test case from UNESCO document, page 27
        underTest = Cast()
        underTest.scanNum = np.array([1])   # Doesn't matter
        underTest.cond = np.array([0])      # Doesn't matter
        underTest.temp = np.array([15])     # Doesn't matter
        underTest.press = np.array([10000]) # Really, really matters
        underTest.calc_depth(30)
        self.assertAlmostEqual(9712.653, underTest.depth[0], 3)

    def test_calc_sv(self):
        # Test cases from Wong & Zhu
        underTest = Cast()
        underTest.scanNum = np.array([1,2,3,4,5,6,7,8,9])
        underTest.cond = np.array([0.0]*9) # not used
        underTest.temp = np.array([0, 40, 0, 40, 0, 40, 0, 40, 10], dtype=np.double)
        underTest.press = np.array([0, 0, 1000, 1000, 0, 0, 1000, 1000, 300], dtype=np.double)*10.0
        underTest.sal = np.array([25, 25, 25, 25, 40, 40, 40, 40, 35], dtype=np.double)
        # Test cases from the UNESCO document
        underTest.calc_sv()
        expected = [1435.711, 1553.314, 1609.905, 1734.533, 1455.779, 1568.053, 1628.334, 1732.374, 1539.320]
        for i in range(0,3):
            self.assertAlmostEqual(expected[i], underTest.sv[i], 1)



def adhoc_test():
    ctdcast = Cast()
    ctdcast.loadUCTD('/home/ivaughn/Desktop/profiles/NA055_00050_20150405_180255_UCTD.asc')
    lat = 28
    #ctdcast.plotRaw()
    ctdcast.autoCrop()
    ctdcast.shiftTemp(1.5)
    #ctdcast.plotRaw()
    ctdcast.calcFields(lat)
    ctdcast.plotProc()
    plt.show()

if __name__ == '__main__':
    unittest.main()
    #adhoc_test()
