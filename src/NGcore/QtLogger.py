#The MIT License (MIT)
# 
#Copyright (c) 2016, Ocean Exploration Trust
#Copyright (c) 2016, J. Ian S. Vaughn
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

from PyQt4 import QtCore
import datetime, traceback, sys

VERB_ALWAYS=   0
VERB_ERROR =   5
VERB_WARN  =  25
VERB_ALERT = 100
VERB_INFO  = 250
VERB_DEBUG = 500

_verb_lookup_table = {'error': VERB_ERROR,
                      'warn' : VERB_WARN,
                      'alert': VERB_ALERT,
                      'info' : VERB_INFO,
                      'debug': VERB_DEBUG}

_verb = VERB_INFO
_print = True
_path = 'log_out.txt'
_fd = None
_mutex = QtCore.QMutex()

def setPrint(onoff):
    global _print

    ''' Set whether or not messages are copied to STDOUT '''
    if onoff:
        _print = True
    else:
        _print = False

def setPath(p):
    ''' Set the logger output path'''

    global _mutex
    global _path
    global _fd

    _mutex.lock()
    _path = p
    if _fd:
        _fd.close()
    _fd = open(_path, 'a')
    _mutex.unlock()

def getPath():
    global _path

    return _path

def setVerbosity(v):
    global _verb 

    if v >= VERB_ERROR:
        _verb = v
    else:
        raise ValueError("Verbosity MUST be above VERB_ERROR (%d), got %d" % (VERB_ERROR, v))

def parseVerbosity(argstr):
    global _verb

    if argstr.lower() in _verb_lookup_table.keys():
        _verb = _verb_lookup_table[argstr.lower()]
        return

    try:
        _verb = int(argstr)
        return
    except ValueError:
        # It's not an int
        pass

    raise ValueError('String \"%s\" not a recognized verbosity level' % argstr)


def getVerbosity():
    global _verb 

    return _verb

def log(msg, verbosity=100):
    ''' Log a message with the specified verbosity to disk.  
    Higher numbers indicate more verboseness'''

    global _mutex
    global _path
    global _fd
    global _verb
    
    # Abort early if verbosity setting is too low
    if verbosity > _verb:
        return

    _mutex.lock()
    if not _fd:
        if not _path:
            _mutex.unlock()
            raise Exception("MUST specify QtLogger path!")
        _fd = open(_path, 'a')
        
    _fd.write(datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S: ') + msg + '\n')
    _fd.flush()

    if _print:
        print(msg)
    _mutex.unlock()


def logException(tag, verbosity=VERB_ERROR):
    ''' Log the exception that's currently being handled.  MUST be called inside an except block'''
    global _mutex
    global _path
    global _fd
    global _verb
    
    # Abort early if verbosity setting is too low
    if verbosity > _verb:
        return

    if not _fd:
        if not _path:
            _mutex.unlock()
            raise Exception("MUST specify QtLogger path!")
        _fd = open(_path, 'a')

    # Re-Implement this here to avoid lots of locking and unlocking
    (exc_type, exc_value, exc_tb) = sys.exc_info()
    err_msg = ''.join(traceback.format_exception(exc_type, exc_value, exc_tb))

    _mutex.lock()
    for line in err_msg.split('\n'):
        _fd.write(datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S: ') \
                + tag + ' ' + line + '\n')
        if _print:
            print (tag + ' ' + line + '\n'),

    _fd.flush()
    _mutex.unlock()
        
    


