#!/usr/bin/python3

# Copyright 2019 Ocean Exploration Trust & Ian Vaughn
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


import unittest
from . import NGnmea
import datetime

class TestFunctions(unittest.TestCase):

    def test_calcChecksum_basic(self):
        line = '$INGGA,050002.28,2545.111712,N,08442.937886,W,2,07,1.2,-7.18,M,-23.27,M,3.0,0138*4B'
        
        calc, included = NGnmea.calcChecksum(line)
        self.assertEqual(calc, int(0x4B))

    def test_calcChecksum_lineEnd(self):
        line = '$INGGA,050002.28,2545.111712,N,08442.937886,W,2,07,1.2,-7.18,M,-23.27,M,3.0,0138*4B\r\n'
        
        calc, included = NGnmea.calcChecksum(line)
        self.assertEqual(calc, int(0x4B))

    def test_calcChecksum_empty(self):
        line = '$INGGA,050002.28,2545.111712,N,08442.937886,W,2,07,1.2,-7.18,M,-23.27,M,3.0,0138*'
        
        calc, included = NGnmea.calcChecksum(line)
        self.assertEqual(calc, int(0x4B))
        self.assertTrue(included is None)

    def test_calcChecksum_noStar(self):
        line = '$INGGA,050002.28,2545.111712,N,08442.937886,W,2,07,1.2,-7.18,M,-23.27,M,3.0,0138'
       
        calc, included = NGnmea.calcChecksum(line)
        self.assertEqual(calc, int(0x4B))
        self.assertTrue(included is None)

    def test_calcChecksum_invalid(self):
        line = 'INGGA,050002.28,2545.111712,N,08442.937886,W,2,07,1.2,-7.18,M,-23.27,M,3.0,0138*4B'
        
        calc, included = NGnmea.calcChecksum(line)
        self.assertEqual(calc, -1)
        self.assertEqual(included, None)

    def test_verifyChecksum_basic(self):
        line = '$INGGA,050002.28,2545.111712,N,08442.937886,W,2,07,1.2,-7.18,M,-23.27,M,3.0,0138*4B\r\n'

        self.assertTrue(NGnmea.verifyChecksum(line))

    def test_verifyChecksum_Invalid(self):
        line = 'INGGA,050002.28,2545.111712,N,08442.937886,W,2,07,1.2,-7.18,M,-23.27,M,3.0,0138*4B\r\n'

        self.assertFalse(NGnmea.verifyChecksum(line))

    def test_verifyChecksum_Bad(self):
        line = '$INGGA,050002.28,2545.111712,N,08442.937886,W,2,07,1.2,-7.18,M,-23.27,M,3.0,0138*FF\r\n'

        self.assertFalse(NGnmea.verifyChecksum(line))

    def test_buildLL_lat(self):
        res = NGnmea.buildLL(12.34567890, isLat=True)
        self.assertEqual(res, '1220.740734,N')

        res = NGnmea.buildLL(-12.34567890, isLat=True)
        self.assertEqual(res, '1220.740734,S')

    def test_buildLL_lon(self):
        res = NGnmea.buildLL(12.34567890, isLat=False)
        self.assertEqual(res, '01220.740734,E')

        res = NGnmea.buildLL(-12.34567890, isLat=False)
        self.assertEqual(res, '01220.740734,W')

    def test_buildLL_smallFieldBug(self):
        # Test with a single zero digit
        res = NGnmea.buildLL(12.020576131, isLat=False)
        self.assertEqual(res, '01201.234568,E')

        res = NGnmea.buildLL(12.020576131, isLat=True)
        self.assertEqual(res, '1201.234568,N')

        # Test with most digits zero
        res = NGnmea.buildLL(12.000000017, isLat=False)
        self.assertEqual(res, '01200.000001,E')

        res = NGnmea.buildLL(12.000000017, isLat=True)
        self.assertEqual(res, '1200.000001,N')

    def test_buildLL_range(self):
        self.assertRaises(ValueError, NGnmea.buildLL, 90.01, isLat=True)
        # This should NOT raise an error
        NGnmea.buildLL(90.01, isLat=False)
        self.assertRaises(ValueError, NGnmea.buildLL, 180.01, isLat=False)

    def test_buildUTCtime_datetime(self):
        testT = datetime.datetime(2014, 10, 9, 8, 7, 6, 123456)
        res = NGnmea.buildUTCtime(testT, precision=6)
        self.assertEqual(res, '080706.123456')

    def test_buildUTCtime_datetime_precision(self):
        testT = datetime.datetime(2014, 10, 9, 8, 7, 6, 123456)
        res = NGnmea.buildUTCtime(testT, precision=6)
        self.assertEqual(res, '080706.123456')
        res = NGnmea.buildUTCtime(testT, precision=5)
        self.assertEqual(res, '080706.12346')
        res = NGnmea.buildUTCtime(testT, precision=4)
        self.assertEqual(res, '080706.1235')
        res = NGnmea.buildUTCtime(testT, precision=3)
        self.assertEqual(res, '080706.123')
        res = NGnmea.buildUTCtime(testT, precision=2)
        self.assertEqual(res, '080706.12')
        res = NGnmea.buildUTCtime(testT, precision=1)
        self.assertEqual(res, '080706.1')
        res = NGnmea.buildUTCtime(testT, precision=0)
        self.assertEqual(res, '080706')

    def test_buildUTCtime_invalid_precision(self):
        testT = datetime.datetime(2014, 10, 9, 8, 7, 6, 123456)
        self.assertRaises(ValueError, NGnmea.buildUTCtime, testT, precision=-1)
        self.assertRaises(ValueError, NGnmea.buildUTCtime, testT, precision=7)

    def test_buildUTCtime_time(self):
        testT = datetime.time(8, 7, 6, 123456)
        res = NGnmea.buildUTCtime(testT, precision=6)
        self.assertEqual(res, '080706.123456')

    def test_buildUTCtime_seconds(self):
        testT = 8*3600 + 7*60 + 6.123456
        res = NGnmea.buildUTCtime(testT, precision=6)
        self.assertEqual(res, '080706.123456')

    def test_buildUTCtime_invalid_seconds(self):
        self.assertRaises(ValueError, NGnmea.buildUTCtime, -1)
        self.assertRaises(ValueError, NGnmea.buildUTCtime, 24*3600+0.001)





class TestBuilder(unittest.TestCase):

    def setUp(self):
        self.underTest = NGnmea.NMEABuilder()
        self.obj = {'heading': 123.456,
                'gps_t': datetime.time(12,34,56,123456),
                'lat': 12.576131502,
                'lon': 178.576131502,
                'quality': 1}
        
    def test_hdt(self):
        line = self.underTest.buildHDT(self.obj)
        self.assertEqual('123.46,T', line)

    def test_hdt_neg(self):
        self.obj['heading'] = -0.01
        self.assertRaises(ValueError, self.underTest.buildHDT, self.obj)

    def test_hdt_high(self):
        self.obj['heading'] = 360.01
        self.assertRaises(ValueError, self.underTest.buildHDT, self.obj)

    def test_full_hdt(self):

        line = self.underTest.buildLine('UT', 'HDT', self.obj)

        self.assertTrue(NGnmea.verifyChecksum(line))
        self.assertEqual('$UTHDT,123.46,T*', line[:-4]) # Don't check the checksum
        self.assertEqual('\r\n', line[-2:]) # Don't check the checksum

    def test_basic_hdg(self):
        line = self.underTest.buildHDG(self.obj)
        self.assertEqual('123.46,0.0,E,0.0,E', line)

    def test_hdg_neg(self):
        self.obj['heading'] = -0.01
        self.assertRaises(ValueError, self.underTest.buildHDG, self.obj)

    def test_hdg_high(self):
        self.obj['heading'] = 360.01
        self.assertRaises(ValueError, self.underTest.buildHDG, self.obj)

    def test_basic_hdg_line(self):

        line = self.underTest.buildLine('UT', 'HDG', self.obj)
        self.assertTrue(NGnmea.verifyChecksum(line))
        self.assertEqual('$UTHDG,123.46,0.0,E,0.0,E*', line[:-4]) #Don't verify the checksum
        self.assertEqual('\r\n', line[-2:]) # Don't check the checksum
        
    def test_talkerTooLong(self):
        self.assertRaises(ValueError, self.underTest.buildLine, 'TOO', 'HDT', self.obj)

    def test_talkerTooShort(self):
        self.assertRaises(ValueError, self.underTest.buildLine, 'T', 'HDT', self.obj)

    def test_talkerNotStr(self):
        self.assertRaises(ValueError, self.underTest.buildLine, 12, 'HDT', self.obj)

    def test_msgTooLong(self):
        self.assertRaises(ValueError, self.underTest.buildLine, 'UT', 'TOOO', self.obj)

    def test_msgTooShort(self):
        self.assertRaises(ValueError, self.underTest.buildLine, 'UT', 'TOOO', self.obj)

    def test_msgNotStr(self):
        self.assertRaises(ValueError, self.underTest.buildLine, 'UT', 25, self.obj)

    def test_msgNotRecognized(self):
        self.assertRaises(ValueError, self.underTest.buildLine, 'UT', 'XXX', self.obj)

    def test_gga(self):
        line = self.underTest.buildLine('UT', 'GGA', self.obj)

        self.assertTrue(NGnmea.verifyChecksum(line))
        self.assertEqual('$UTGGA,123456.123,1234.567890,N,17834.567890,E,1,,,,,,,,*', line[:-4]) # Don't check the checksum
        self.assertEqual('\r\n', line[-2:]) # Don't check the checksum

    def test_gga_checksum_case(self):
        line = self.underTest.buildLine('UT', 'GGA', self.obj)

        self.assertTrue(NGnmea.verifyChecksum(line))
        self.assertEqual('$UTGGA,123456.123,1234.567890,N,17834.567890,E,1,,,,,,,,*5E\r\n', line) # DO Check the checksum!




if __name__ == '__main__':
    unittest.main()


