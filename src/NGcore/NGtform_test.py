# -*- coding: utf-8 -*-

import unittest
from . import NGtform

import numpy as np
import math

NUMPY_MATRIX_TYPE = np.matrixlib.defmatrix.matrix

class Point3Test(unittest.TestCase):
    def testNoneCons(self):
        pt = NGtform.Point()
        
        self.assertTrue((np.matrix([[0],[0],[0],[1]]) == pt._arr).all())
        self.assertTrue(pt._arr.shape[1] == 1)
        
    def test3Vec(self):
        
        vec = [1,2,3]
        
        pt = NGtform.Point(vec)
        
        self.assertTrue((np.matrix([[1],[2],[3],[1]]) == pt._arr).all())
        self.assertTrue(pt._arr.shape[1] == 1)
        
    def test3Hvec(self):
        vec = [2,4,6,2]
        
        pt = NGtform.Point(vec)
        
        self.assertTrue((np.matrix([[1],[2],[3],[1]]) == pt._arr).all())
        self.assertTrue(pt._arr.shape[1] == 1)
        
    def testInvalidRows(self):
        self.assertRaises(ValueError, NGtform.Point, [1,2])

class PoseTest(unittest.TestCase):
    def testTformNoInit(self):
        pose1 = NGtform.Pose([6,5,4,0.1,0.2,0.3])
        offset = NGtform.Pose()
        pose2 = pose1.oplus(offset)

        self.assertTrue((np.matrix([[6],[5],[4],[0.1],[0.2],[0.3]]) == pose2._arr).all())
        self.assertIsInstance(pose2._arr, NUMPY_MATRIX_TYPE)

    def testTformIdentity(self):
        pose1 = NGtform.Pose([6,5,4,0.1,0.2,0.3])
        offset = NGtform.Pose.Identity()
        pose2 = pose1.oplus(offset)

        self.assertTrue((np.matrix([[6],[5],[4],[0.1],[0.2],[0.3]]) == pose2._arr).all())
        self.assertIsInstance(pose2._arr, NUMPY_MATRIX_TYPE)

    def testTformNontrivial(self):
        pose1 = NGtform.Pose([-0.433592022305684, 0.342624466538650,\
                3.578396939725760, 2.769437029884877,\
                -1.349886940156521, 3.034923466331855])


        offset = NGtform.Pose([0.725404224946106, -0.063054873189656,\
                0.714742903826096, -0.204966058299775,\
                -0.124144348216312, 1.489697607785465])

        expected = NGtform.Pose([-1.238407122015125, 0.631109832420825,\
                4.135257311923219, -2.001810051773473,\
                -0.132347150133326, 1.239556875157120])

        pose2 = pose1.oplus(offset)
        maxErr = np.max(np.abs(expected._arr - pose2._arr))

        self.assertTrue(maxErr < 1.0e-14)
        self.assertIsInstance(pose2._arr, NUMPY_MATRIX_TYPE)

    def testTformJacobian(self):
        pose1 = NGtform.Pose([-0.433592022305684, 0.342624466538650,\
                3.578396939725760, 2.769437029884877,\
                -1.349886940156521, 3.034923466331855])


        offset = NGtform.Pose([0.725404224946106, -0.063054873189656,\
                0.714742903826096, -0.204966058299775,\
                -0.124144348216312, 1.489697607785465])

        expected = NGtform.Pose([-1.238407122015125, 0.631109832420825,\
                4.135257311923219, -2.001810051773473,\
                -0.132347150133326, 1.239556875157120])

        expectedJacobian = np.matrix([\
[ 1.000000000000000,                   0,                   0,  -0.268483917821746,  -0.553695308491705,  -0.288485365882175,  -0.217871591199443,   0.451950111428376,  -0.865027783674086,                   0,                   0,                   0],\
[                 0,   1.000000000000000,                   0,  -0.663932903217221,   0.059287262360207,  -0.804815099709442,   0.023328733312666,   0.888477792694568,   0.458326283438573,                   0,                   0,                   0],\
[                 0,                   0,   1.000000000000000,  -0.044077448875375,  -0.830954895677976,                   0,   0.975698590728843,   0.079676274187910,  -0.204117494059876,                   0,                   0,                   0],\
[                 0,                   0,                   0,  -0.049225082155692,  -0.983490625657794,                   0,                   0,                   0,                   0,   1.000000000000000,   0.129738592655452,   0.153433131465043],\
[                 0,                   0,                   0,   0.213614949664039,  -0.222687437793680,                   0,                   0,                   0,                   0,                   0,  -0.224127498808220,   0.967059601195344],\
[                 0,                   0,                   0,   0.982194388097445,   0.129782532368084,   1.000000000000000,                   0,                   0,                   0,                   0,  -0.983157650991024,  -0.224364703165077]])

        pose2, jacobian = pose1.oplus(offset,jacobian=True)

        self.assertTrue(jacobian.shape == (6,12) )
        maxPoseErr = np.max(np.abs(expected._arr - pose2._arr))
        maxJacobErr = np.max(np.abs(jacobian - expectedJacobian))
        
        self.assertTrue(maxPoseErr < 1.0e-14)
        self.assertIsInstance(pose2._arr, NUMPY_MATRIX_TYPE)

        self.assertTrue(maxJacobErr < 1.0e-14)
        self.assertIsInstance(jacobian, NUMPY_MATRIX_TYPE)

class Transform3Test(unittest.TestCase):
    
    def testTfromNoInit(self):
        pt = NGtform.Point([1,0,0])
        tform = NGtform.Transform()
        pt2 = tform.tform(pt)
        
        self.assertTrue((np.matrix([[1],[0],[0],[1]]) == pt2._arr).all())
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)

        
    def testTformIdentity(self):
        pt = NGtform.Point([1,0,0])
        tform = NGtform.Transform.Identity()
        pt2 = tform.tform(pt)
        
        self.assertTrue((np.matrix([[1],[0],[0],[1]]) == pt2._arr).all())
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testTformTrans(self):
        pt = NGtform.Point([1,0,0])
        tform = NGtform.Transform.Identity()
        tform.setTrans([1,2,3])
        
        pt2 = tform.tform(pt)
        
        self.assertTrue((np.matrix([[2],[2],[3],[1]]) == pt2._arr).all())
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testTformTransNParray(self):
        pt = NGtform.Point([1,0,0])
        tform = NGtform.Transform.Identity()
        tform.setTrans(np.array([1,2,3]))
        
        pt2 = tform.tform(pt)
        
        self.assertTrue((np.matrix([[2],[2],[3],[1]]) == pt2._arr).all())
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testGetTrans(self):
        
        tform = NGtform.Transform.Identity()
        tform.setTrans([1,2,3])
        
        self.assertTrue((np.matrix([1, 2, 3]).transpose() == tform.getTrans()).all())
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testTransShapeErrSmall(self):
        self.assertRaises(ValueError, NGtform.Transform.Translation, [1,2])

    def testTransShapeErrBig(self):
        self.assertRaises(ValueError, NGtform.Transform.Translation, [1,2,3,4])

        
        
    def testStaticTrans(self):
        tform = NGtform.Transform.Translation([1,2,3])
        pt = NGtform.Point([1,0,0])
        
        pt2 = tform.tform(pt)
        
        self.assertTrue((np.matrix([[2],[2],[3],[1]]) == pt2._arr).all())
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testRotXdeg(self):
        tform = NGtform.Transform.RotXdeg(90)
        pt = NGtform.Point([1,1,0])
        
        pt2 = tform.tform(pt)
        
        self.assertLess(np.abs(((np.matrix([[1],[0],[1],[1]]) - pt2._arr))).max(), 1e-10)
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testRotXrad(self):
        tform = NGtform.Transform.RotXrad(math.pi/2)
        pt = NGtform.Point([1,1,0])
        
        pt2 = tform.tform(pt)
        
        self.assertLess(np.abs(((np.matrix([[1],[0],[1],[1]]) - pt2._arr))).max(), 1e-10)
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testRotYdeg(self):
        tform = NGtform.Transform.RotYdeg(90)
        pt = NGtform.Point([1,1,0])
        
        pt2 = tform.tform(pt)
        
        self.assertLess(np.abs(((np.matrix([[0],[1],[1],[1]]) - pt2._arr))).max(), 1e-10)
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testRotYrad(self):
        tform = NGtform.Transform.RotYrad(math.pi/2)
        pt = NGtform.Point([1,1,0])
        
        pt2 = tform.tform(pt)
        
        self.assertLess(np.abs(((np.matrix([[0],[1],[1],[1]]) - pt2._arr))).max(), 1e-10)
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testRotZdeg(self):
        tform = NGtform.Transform.RotZdeg(90)
        pt = NGtform.Point([1,0,1])
        
        pt2 = tform.tform(pt)
        
        self.assertLess(np.abs(((np.matrix([[0],[1],[1],[1]]) - pt2._arr))).max(), 1e-10)
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testRotZrad(self):
        tform = NGtform.Transform.RotZrad(math.pi/2)
        pt = NGtform.Point([1,0,1])
        
        pt2 = tform.tform(pt)
        
        self.assertLess(np.abs(((np.matrix([[0],[1],[1],[1]]) - pt2._arr))).max(), 1e-10)
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testRotXThenY(self):
        rotx = NGtform.Transform.RotXdeg(90)
        roty = NGtform.Transform.RotYdeg(90)

        comp = roty.dot(rotx)  # First apply rotx, then roty
                               # after rotx, should be [0,0,1]
                               # Then, rotating about y should give [-1,0,0]
        
        pt = NGtform.Point([0,1,0])
        pt2 = comp.tform(pt)
        
        self.assertLess(np.abs(((np.matrix([[-1],[0],[0],[1]]) - pt2._arr))).max(), 1e-10)
        self.assertIsInstance(comp._mat, NUMPY_MATRIX_TYPE)
        
    def testRotXThenYThenTrans(self):
        rotx = NGtform.Transform.RotXdeg(90)
        roty = NGtform.Transform.RotYdeg(90)
        trans = NGtform.Transform.Translation([2,0,0])

        comp = roty.dot(rotx)     # First apply rotx, then roty
                                  # after rotx, should be [0,0,1]
                                  # Then, rotating about y should give [-1,0,0]
        comp2 = trans.dot(comp)   # Now add translation by [2,0,0], giving [1,0,0]
        
        pt = NGtform.Point([0,1,0])
        pt2 = comp2.tform(pt)
        
        self.assertLess(np.abs(((np.matrix([[1],[0],[0],[1]]) - pt2._arr))).max(), 1e-10)
        self.assertIsInstance(comp2._mat, NUMPY_MATRIX_TYPE)
        
    def testRPHfossen_RPonly(self):
        
        tform = NGtform.Transform.RPHfossenDeg([30,30,0])
        pt = NGtform.Point([0,1,0])
        # Got the expected result from Louis Whitcomb and Ryan Eustance's rotxyz.m
        # We need to match that implementation, so we go with that
        expected = np.matrix([0.25000000, 0.866025403784439, 0.433012701892219, 1.0]).transpose()
        
        pt2 = tform.tform(pt)
        
        self.assertLess(np.abs(expected - pt2._arr).max(), 1e-8)
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testRPHfossen(self):
        
        tform = NGtform.Transform.RPHfossenDeg([30,30,90])
        pt = NGtform.Point([0,1,0])
        # Got the expected result from Louis Whitcomb and Ryan Eustance's rotxyz.m
        # We need to match that implementation, so we go with that
        expected = np.matrix([-0.866025403784439, 0.250000000000000, 0.433012701892219, 1.0]).transpose()
        
        pt2 = tform.tform(pt)
        
        self.assertLess(np.abs(expected - pt2._arr).max(), 1e-8)
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testXYZRPHfossenDeg(self):
        tform = NGtform.Transform.XYZRPHfossenDeg([0.866025403784439,
                    -0.250000000000000, -0.433012701892219, 30, 30, 90])
        pt = NGtform.Point([0,1,0])
        expected = np.matrix([0,0,0,1]).transpose()
        
        pt2 = tform.tform(pt)
        self.assertLess(np.abs(expected - pt2._arr).max(), 1e-8)
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
        
    def testXYZRPHfossenDegInt(self):
        tform = NGtform.Transform.XYZRPHfossenDeg([0,0,0, 30, 30, 90])
        pt = NGtform.Point([0,1,0])
        expected = np.matrix([-0.866025403784439,
                    0.250000000000000, 0.433012701892219,1]).transpose()
        
        pt2 = tform.tform(pt)
        self.assertLess(np.abs(expected - pt2._arr).max(), 1e-8)
        self.assertIsInstance(tform._mat, NUMPY_MATRIX_TYPE)
    
def main():
    unittest.main()
    
if __name__ == '__main__':
    main()
