#!/usr/bin/python

#The MIT License (MIT)
# 
#Copyright (c) 2016, Ocean Exploration Trust
#Copyright (c) 2016, J. Ian S. Vaughn
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import argparse

class ignore(argparse.Action):
  # we create an action that does nothing, so the Qt args do nothing
  def __call__(self, parser, namespace, values, option_string=None):
    pass

class QtArgumentParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        kwargs['add_help'] = False
        super(QtArgumentParser, self).__init__(*args, **kwargs)

        # we now have to add all of the options described at 
        # http://qt-project.org/doc/qt-4.8/qapplication.html#QApplication
        # but have them do nothing - in order to have them show up in the help list

        # add this to the list if Qt is a debug build (How to detect this?)
        self.add_argument("-nograb", action=ignore,
                      help="don't grab keyboard/mouse for debugging")

        # add these to the list if Qt is a debug build for X11
        self.add_argument("-dograb", action=ignore,
                      help="grab keyboard/mouse for debugging")
        self.add_argument("-sync", action=ignore,
                      help="run in synchronous mode for debugging")

        # add all the standard args that Qt will grab on all platforms
        self.add_argument("-reverse", action=ignore,
                      help="run program in Right-to-Left mode")
        # an example -- there are 10 such items in the docs for QApplication

        # then we need to figure out if we're running on X11 and add these
        self.add_argument("-name", action=ignore,
                      help="sets the application name")
        # an example -- there are 13 such items in the docs

        # reimplement help (which we disabled above) so that -help works rather 
        # than --help; done to be consistent with the style of args Qt wants
        self.add_argument("-h", "-help", action='help',
                      help="show this help message and exit")

    def parse_app(self, app):
        argv = [str(i) for i in app.arguments()] 
        return self.parse_args(argv[1:])

from PyQt4 import QtGui, QtCore 

def quit_clean(*args):
    print ( 'Got SIGINT, quitting cleanly...' )
    QtGui.QApplication.quit()

import json, datetime

class DateTimeEncoder(json.JSONEncoder):
    date_fmt = '%Y-%m-%dT%H:%M:%S'
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.strftime(DateTimeEncoder.date_fmt)
        return json.JSONEncoder.default(self, o)
