#!/usr/bin/python3

# Copyright 2019 Ocean Exploration Trust & Ian Vaughn
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


import operator
#import string
import datetime
from functools import reduce

def calcChecksum(line):
    ''' Calculate the NMEA checksum.  Returns (calculated checksum, 
    included checksum).  If the string does not included a checksum,
    then the included checksum is returned as None'''
    if line[0] != '$':
        return -1, None

    if '*' in line:
        nmeadata, chksum = line.strip().split('*')
        if chksum:
            chksum = int(chksum, 16) # Convert from string to number
        else:
            chksum = None
    else:
        nmeadata = line.strip()
        chksum = None

    calc_chksum = reduce(operator.xor, (ord(s) for s in nmeadata[1:]),0)


    return calc_chksum, chksum

def verifyChecksum(line):
    ''' Return TRUE if the line's checksum verifie correctly '''
    calc, included = calcChecksum(line)
    return calc == included

# Convert to decimal lat/lon with the sign 
# from the given direction letter
def parseLL(ddmm, d):
    ''' Parse a ddmm.mmm,D chunk from a NMEA lat/lon message correctly.
    First argument is the [d]ddmm.mmmm format NMEA uses (arbitrary precision), and
    the second is the direction flag (one of N,S,E,W).  Returns as +/-decimal degrees'''
    splitat = str.find(ddmm, '.')-2
    dd = float(ddmm[:splitat]) + float(ddmm[splitat:])/60.0
    
    if d == 'N':
        return dd
    elif d == 'S':
        return -dd
        
    if d == 'E':
        return dd
    elif d == 'W':
        return -dd
        
    #print d
    raise ValueError('Direction flag must be one of [NSEW]')

def buildLL_bits(degD, isLat):
    ''' Build the elements of a lat/lon string; deg/min/dir'''

    mag = abs(degD)
    deg = int(mag)
    minutes = (mag - deg)*60

    if isLat:
        if mag > 90.0:
            raise ValueError('Latitude must be between +/-90 degrees!')
        if degD >= 0:
            flag = 'N'
        else:
            flag = 'S'
    else:
        if mag > 180.0:
            raise ValueError('Longitude must be in the range +/-180 degrees!')
        if degD >= 0:
            flag = 'E'
        else:
            flag = 'W'

    return (deg, minutes, flag)

def buildLL(degD, isLat, precision=6):
    ''' Build a latitude or longitude ddmm.mmmm string. isLat=True treats this
    as latitude, False treats it as longitude'''

    (deg, minutes, flag) = buildLL_bits(degD, isLat)
    if isLat:
        fmtstr = '%%02d%%0%d.%df,%%s' % (precision + 3, precision)
    else:
        fmtstr = '%%03d%%0%d.%df,%%s' % (precision + 3, precision)

    return fmtstr % (deg, minutes, flag)


    
def parseUTCtime(timestr):
    ''' Parse the UTC time string from a GPS message to subsecond precision'''
    hours = float(timestr[0:2])
    minutes = float(timestr[2:4])
    seconds = float(timestr[4:])
    
    return seconds + 60*minutes + 3600*hours

def buildUTCtime(timeObj, precision=2):
    if not (isinstance(timeObj, datetime.datetime) or isinstance(timeObj, datetime.time)):
        timeSec = float(timeObj)
        if timeSec < 0 or timeSec > 24*3600:
            raise ValueError('buildUTCtime requires datetime, time, or seconds since midnight')
        hours = int(timeSec/3600)
        timeSec -= hours*3600
        minutes = int((timeSec)/60)
        timeSec -= minutes*60
        seconds = int(timeSec)
        timeSec -= seconds
        microseconds = int(timeSec*1.0e6)
        timeObj = datetime.time(hours,minutes,seconds,microseconds)

    precision = int(precision)

    basePart = timeObj.strftime('%H%M%S')
    if precision < 0 or precision > 6:
        raise ValueError('Precision must be in [0,6]')

    if precision == 0:
        return basePart

    decimalPart = ('%%.%df' % precision) % (timeObj.microsecond / 1.0e6)
    return basePart + decimalPart[1:]

class NMEAParser(object):
    def __init__(self,verbose=False, allowDVLNAVgga=False):
        self.verbose=verbose
        self.allowDVLNAVgga=allowDVLNAVgga
        
        self.parserDict = {}
        # Gyro, Seapath
        self.parserDict['HDT'] = self.parseHDT
        
        # seapath, hypack, DP (via .nav)
        self.parserDict['GGA'] = self.parseGGA
        
        # Hypack
        self.parserDict['HDG'] = self.parseHDG

        # Gyro        
        self.parserDict['ROT'] = self.parseROT
        
        # Seapath
        self.parserDict['VTG'] = self.parseVTG
        
        # Time of day, etc
        self.parserDict['ZDA'] = self.parseZDA
        
        # Weather, from DP system
        self.parserDict['MWV'] = self.parseMWV
        
        # Something from the gyro
        self.parserDict['LAN'] = None
        
        # Messages to ignore
        
    def parseHDT(self, parts):
        
        if len(parts) < 3:
            print('HDT string %d, rather than %d parts' % (len(parts), 3))
            return None

        ret = {'heading': float(parts[1]), 'true': parts[2] }
        
        return ret
        
    def parseGGA(self, parts):

        ## Definitions of the various quality values
        qstr = ['fix not available',
                'GPS fix',
                'Differential GPS fix',
                'PPS fix',
                'Real Time Kinematic',
                'Float RTK',
                'estimated (dead reckoning)',
                'Manual input mode',
                'Simulation mode',]        
        
        if self.allowDVLNAVgga:
            if len(parts) < 6:
                print('GGA string had only %d parts!' % len(parts))
                return None
        elif len(parts) < 7:
            print('GGA string had only %d parts!' % len(parts))
            return None
            
        lat = parseLL(parts[2], parts[3])
        lon = parseLL(parts[4], parts[5])
        
        ret = {'lat': lat, 'lon': lon}

        # Time is not reported by some GPSs (i.e., Gyro)
        timepart = parts[1]
        if len(timepart) > 0:
            ret['gps_t'] = parseUTCtime(timepart)
            
        if not (self.allowDVLNAVgga and len(parts) == 6):
            quality = int(parts[6])
            ret['quality'] = quality
            if quality < len(qstr):
                ret['quality_str'] = qstr[quality]
            
        ## The following fields are optional, so don't
        ## complain if they error
        try:
            ret['num_sats'] = int(parts[7])
        except:
            pass
            
        try:
            ret['hdop'] = float(parts[8])
        except:
            pass
        try:
            ret['ant_geoid_alt'] = float(parts[9])
        except:
            pass
        try:
            ret['ant_alt_units'] = parts[10]
        except:
            pass
        try:
            ret['geoid'] = float(parts[11])
        except:
            pass
        try:
            ret['geoid_units'] = parts[12]
        except:
            pass
        try:
            ret['diff_age'] = float(parts[13])
        except:
            pass
        try:
            ret['diff_station'] = parts[14]
        except:
            pass
                    
        return ret

    def parseHDG(self, parts):
        
        if len(parts) < 2:
            print('HDG string had %d, rather than at least 2, parts!' % len(parts))
            return None
        
        ret = {'heading': float(parts[1])}

        # These fields are also optional
        try:
            ret['mag_dev'] = float(parts[2])
        except:
            pass
        
        try:
            ret['mag_dev_dir'] = parts[3]
        except:
            pass
        
        try:
            ret['mag_var'] = float(parts[4])
        except:
            pass
        
        try:
            ret['mag_var_dir'] = parts[5]
        except:
            pass
        
        return ret

    def parseROT(self, parts):
        if len(parts) < 3:
            print('ROT string %d, rather than 3 parts' % len(parts))
            return None

        ret = {'turnrate': float(parts[1]), 'status': parts[2] }
        
        return ret
        
    def parseVTG(self, parts):
        if len(parts) < 5:
            print('VTG message too short for form 1!') 
            return None
            
        
        ret = {}
        if len(parts) == 5:
            print('Using VTG Form 1!')
            
            try:
                ret['course_true'] = float(parts[1])
            except:
                pass
            
            try: 
                ret['course_mag'] = float(parts[2])
            except:
                pass
            
            try: 
                ret['speed_kt'] = float(parts[3])
            except:
                pass
            
            try: 
                ret['speed_km'] = float(parts[4])
            except:
                pass
            
            ## Check for empty
            if not ret:
                return None
                
            return ret
        
        if parts[2] != 'T':
            print('Expected form 2 of VTG, but field 2 was not \"T\"!')
            return None
            
        try:
            ret['course_true'] = float(parts[1])
        except:
            pass
            
        try: 
            ret['course_mag'] = float(parts[3])
        except:
            pass
        
        # If there's no course direction, don't return!
        if not ret:
            return None
            
        try: 
            ret['speed_kt'] = float(parts[5])
        except:
            pass
        
        try: 
            ret['speed_km'] = float(parts[7])
        except:
            pass
        
        try: 
            ret['faa_mode'] = float(parts[9])
        except:
            pass
        
        return ret
        

    def parseZDA(self, parts):
        if len(parts) < 7:
            print('ZDA msg has only %d < 7 parts!' % len(parts))
            return None
        
        gps_t = parseUTCtime(parts[1])
        day = int(parts[2])
        month = int(parts[3])
        year = int(parts[4])
        
        t = 0
        ret = {'gps_t': gps_t, 'day': day, 'month': month, 'year': year, 'gps_tstamp': t}
        
        try:
            tzone_hr = int(parts[5])
            tzone_min = int(parts[6])
            ## So the spec says tzone_min has the same sign as tzone_hr,
            ## but doesn't include that anywhere.  What.  The.   Bleep.
            ret['tz'] = tzone_hr + math.copysign(tzone_min, tzone_hr)
        except:
            pass
                
        
        return ret
        

    def parseMWV(self, parts):
        if len(parts) < 5:
            print('MWV msg has only %d < 5 parts!' % len(parts)) 
            return None
        
        # Treat all fields as optional
        ret = {}
        try: 
            ret['wind_ang'] = float(parts[1])
        except:
            pass
        try: 
            ret['wind_ang_ref'] = parts[2]
        except:
            pass
        try: 
            ret['wind_spd'] = float(parts[3])
        except:
            pass
        try: 
            ret['wind_spd_units'] = parts[4]
        except:
            pass
        
        try: 
            ret['valid'] = (parts[5] == 'A')
            ret['valid_str'] = parts[5]
        except:
            pass
        
        # don't return empty 
        if ret:
            return ret
            
        return None
        
    def parseLine(self, line):
        parts = line.split('*')[0].split(',')
        idpart = parts[0]
        if idpart[0] != '$':
            return None
            
        talker = idpart[1:3]
        msgid = idpart[3:6]
        
        if msgid in list(self.parserDict.keys()):
            func = self.parserDict[msgid]
            if func is None:
                return None
            else:
                ret = func(parts)
        else:
            print('Unrecognized msg type \"%s\"' % msgid)
            return None
        
        if ret is None:
            return None
            
        ret['talker'] = talker
        ret['msg'] = msgid
        ret['idstr'] = idpart
        
        return ret

class NMEABuilder(object):

    def __init__(self,verbose=False):
        self.verbose=verbose
        
        self.builderDict = {}
        self.builderDict['HDT'] = self.buildHDT
        self.builderDict['HDG'] = self.buildHDG
        self.builderDict['GGA'] = self.buildGGA
        
        # Stuff the parser does we should do too someday
        #self.parserDict['HDG'] = self.parseHDG
        #self.parserDict['ROT'] = self.parseROT
        #self.parserDict['VTG'] = self.parseVTG
        #self.parserDict['ZDA'] = self.parseZDA
        #self.parserDict['MWV'] = self.parseMWV
        #self.parserDict['LAN'] = None


    def buildLine(self, talker, msg, obj):
        ''' Build a NMEA object with the specified message / talker from the 
        given dictionary of data. Fields match those from NMEAParser.  A 2-character
        talker ID is used to identify the talker, and a 3-character message ID is
        used to determine which message to send.  This allows a single object 
        representing a vessel's state to '''

        if not isinstance(talker, str) or len(talker) != 2:
            raise ValueError('Talker must be a two-item-long string!')

        if not isinstance(msg, str) or len(msg) != 3:
            raise ValueError('MSGID must be a three-item-long string!')

        if msg in list(self.builderDict.keys()):
            func = self.builderDict[msg]
            if func is None:
                raise ValueError('Unrecognized msg type \"%s\"' % msg)
            else:
                payload = func(obj)
        else:
            raise ValueError('Unrecognized msg type \"%s\"' % msg)

        if payload is None:
            return None

        rawLine = '$' + talker + msg + ',' + payload + '*'
        rawLine[1:]
        chksum,junk = calcChecksum(rawLine)

        return rawLine + ('%02X' % chksum) + '\r\n'

    def buildHDT(self, obj):
        hdg = obj['heading']
        if hdg > 360.0 or hdg < 0.0:
            raise ValueError('Heading must be in [0,360]')
        return '%.2f,T' % hdg


    def buildGGA(self, obj):
        ret = '%s,%s,%s,%d' % (buildUTCtime(obj['gps_t'], precision=3),
                            buildLL(obj['lat'], isLat=True, precision=6),
                            buildLL(obj['lon'], isLat=False, precision=6),
                            obj['quality'])

        if 'num_sats' in obj:
            ret += (',%d' % obj['num_sats'])
        else:
            ret += ','

        if 'hdop' in obj:
            ret += (',%f' % obj['hdop'])
        else:
            ret += ','

        if 'ant_geoid_alt' in obj:
            ret += (',%.3f' % obj['ant_geoid_alt'])
        else:
            ret += ','

        if 'ant_alt_units' in obj:
            ret += (',%s' % obj['ant_alt_units'])
        else:
            ret += ','

        if 'geoid' in obj:
            ret += (',%.3f' % obj['geoid'])
        else:
            ret += ','

        if 'geoid_units' in obj:
            ret += (',%s' % obj['geoid_units'])
        else:
            ret += ','

        if 'diff_age' in obj:
            ret += (',%f' % obj['diff_age'])
        else:
            ret += ','

        if 'diff_station' in obj:
            ret += (',%s' % obj['diff_station'])
        else:
            ret += ','

        return ret

    def buildHDG(self, obj):
        hdg = obj['heading']
        if hdg > 360.0 or hdg < 0.0:
            raise ValueError('Heading must be in [0,360]')
        ret = '%.2f,' % hdg
        if 'mag_dev' in obj:
            raise Exception("Magnetic deviation in HDG not implemented!")
        else:
            ret += '0.0,E,'

        if 'mag_var' in obj:
            raise Exception("Magnetic variation in HDG not implemented!")
        else:
            ret += '0.0,E'
        
        return ret

 
