# -*- coding: utf-8 -*-
# Copyright 2019 Ocean Exploration Trust & Ian Vaughn
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


import unittest
import re
import numpy as np
import math
import copy
import json
import datetime

class ScalarLimits(object):
    ''' A class to collect the limits of a scalar '''
    def __init__(self, init_value=None):
        self.low = copy.copy(init_value)
        self.high = copy.copy(init_value)

    def update(self, value):
        if self.low is None or value < self.low:
            self.low = copy.copy(value)

        if self.high is None or value > self.high:
            self.high = copy.copy(value)

    def isValid(self):
        return self.low is not None and self.high is not None

    def getHigh(self):
        return self.high

    def getLow(self):
        return self.low


class AngleLimits(ScalarLimits):
    ''' A class to collect the limits of a scalar '''
    def __init__(self, init_value=None,wrap='0deg'):
        super(AngleLimits, self).__init__(init_value=init_value)

        if wrap == '0deg':
            self.norm = self.norm_0deg
        elif wrap == 'pm180deg':
            self.norm = self.norm_pm180deg

    def fix(self, value):
        ''' Convert to be between +/- 180.0 '''
        if value > 180.0:
            return value - 360.0

        if value < -180.0:
            return value + 360.0

        return value

    def norm_0deg(self, value):
        return value % 360.0
        #if value < 0:
        #    return value + 360.0
        #return value

    def norm_pm180deg(self, value):
        tmp = value % 360.0
        if tmp > 180.0:
            tmp -= 360.0
        return tmp

    def update(self, value):
        value = self.norm(value)

        # Update low
        if self.low is None or self.high is None:
            self.low = value
            self.high = value
            return


        delta_low = self.fix(value - self.low)
        delta_high = self.fix(value - self.high)
        
        # Ok, decide if this is the upper or lower based on which it's closer to
        if self.low == self.high:
            # Corner case!
            if delta_low < 0.0:
                self.low = value
            else:
                self.high = value
            return

        if abs(delta_low) < abs(delta_high):
            # Ok, we're going to update low, but only the 
            # new angle is to the left of the current value
            if delta_low < 0.0:
                self.low = value

        else:
            # Ok, we're going to update high, but only the 
            # new angle is to the right of the current value
            if delta_high > 0.0:
                self.high = value
            

    def update_pm180deg(self, value):
        pass

def mongoSanatize(s):
    '''Sanataize a mongo name string (probably a collection)
    by replacing everything not in [a-zA-Z0-9_] with '_' '''
    rx = re.compile('([^\w])')    
    
    return rx.sub('_', s)
                
def xyzrphToBson(xyzrph):
    return xyzrph
    
def covarToBson(covar):
    ret = {}
    ret['rows'] = covar.shape[0]
    ret['cols'] = covar.shape[1]
    
    idx=0
    data = [0] * (covar.shape[0] * covar.shape[1])
    for r in range(0, covar.shape[0]):
        for c in range(0, covar.shape[1]):
            data[idx] = float(covar[r,c])
            idx += 1
    
    ret['data'] = data
    
    return ret

    

def rotation_matrix(rph):    
    cr = math.cos(rph[0])
    sr = math.sin(rph[0])
    cp = math.cos(rph[1])
    sp = math.sin(rph[1])
    ch = math.cos(rph[2])
    sh = math.sin(rph[2])
    
    rotmat = np.matrix(np.zeros((3,3)))
    
    rotmat[0,0] = ch*cp
    rotmat[0,1] = (-sh*cr + ch*sp*sr)
    rotmat[0,2] = ( sh*sr + ch*sp*cr)
    
    rotmat[1,0] = sh*cp
    rotmat[1,1] = ( ch*cr + sh*sp*sr)
    rotmat[1,2] = (-ch*sr + sh*sp*cr)
    
    rotmat[2,0] = -sp
    rotmat[2,1] = cp*sr
    rotmat[2,2] = cp*cr
    
    return rotmat

def xyzrph_to_affine4d(xyzrph, degrees=False):    
    tform = np.array(np.zeros((4,4)))
    
    # [possibly] convert xyzrph from degrees to radians
    if degrees:
        tform[0:3,0:3] = rotation_matrix(np.array(xyzrph[3:6])*math.pi/180.0)
    else:
        tform[0:3,0:3] = rotation_matrix(np.array(xyzrph[3:6]))

    tform[0:3,3] = np.array(xyzrph[0:3])
        
    tform[3,3] = 1
    
    return np.matrix(tform)

class DateTimeEncoder(json.JSONEncoder):
    date_fmt = '%Y-%m-%dT%H:%M:%S'
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.strftime(DateTimeEncoder.date_fmt)
        return json.JSONEncoder.default(self, o)
    
class TestMongoSanatize(unittest.TestCase):
    def test_basic(self):
        testStr = 'some#&*Y()_%&!@#_thing123'
        goodStr = 'some___Y_________thing123'
        gotStr = mongoSanatize(testStr)

        self.assertEqual(gotStr, goodStr)

class TestAngleLimits(unittest.TestCase):

    def testFixSimple(self):
        ut =  AngleLimits()
        self.assertEqual(ut.fix(-350), 10)
        self.assertEqual(ut.fix(10), 10)

    def testFixClose(self):
        ut =  AngleLimits()
        self.assertEqual(ut.fix(-179.5), -179.5)
        self.assertEqual(ut.fix(179.5), 179.5)

    def testFixOver(self):
        ut =  AngleLimits()
        self.assertEqual(ut.fix(-180.5), 179.5)
        self.assertEqual(ut.fix(180.5), -179.5)

    def testNorm0deg_basic(self):
        ut =  AngleLimits()
        self.assertEqual(ut.norm_0deg(5), 5)
        self.assertEqual(ut.norm_0deg(179.5), 179.5)
        self.assertEqual(ut.norm_0deg( -0.5), 359.5)

    def testNorm0deg_extreme(self):
        ut =  AngleLimits()
        self.assertEqual(ut.norm_0deg( 360.5),   0.5)
        self.assertEqual(ut.norm_0deg(-360.5), 359.5)

    def testNormPM180deg_basic(self):
        ut =  AngleLimits()
        self.assertEqual(ut.norm_pm180deg(5), 5)
        self.assertEqual(ut.norm_pm180deg( -0.5), -0.5)
        self.assertEqual(ut.norm_pm180deg(179.5), 179.5)
        self.assertEqual(ut.norm_pm180deg(180.5), -179.5)
        self.assertEqual(ut.norm_pm180deg(-179.5), -179.5)
        self.assertEqual(ut.norm_pm180deg(-180.5), 179.5)

    def testNormPM180deg_extreme(self):
        ut =  AngleLimits()
        self.assertEqual(ut.norm_0deg( 360.5),   0.5)
        self.assertEqual(ut.norm_0deg(-360.5), 359.5)

    def testSimple(self):
        ut =  AngleLimits()
        ut.update(5)
        ut.update(7)
        self.assertEqual(5, ut.getLow())
        self.assertEqual(7, ut.getHigh())

    def testWrapZero(self):
        ut =  AngleLimits(wrap='0deg')
        ut.update(355)
        ut.update(7)
        self.assertEqual(355, ut.getLow())
        self.assertEqual(7, ut.getHigh())

    def testWrapZeroBigRight(self):
        ut =  AngleLimits(wrap='0deg')
        ut.update(355.0)
        for i in range(0, 205, 5):
            ut.update(float(i))

        self.assertEqual(355, ut.getLow())
        self.assertEqual(200, ut.getHigh())

    def testWrapZeroBigLeft(self):
        ut =  AngleLimits(wrap='0deg')
        ut.update(5.0)
        for i in range(0, -205, -5):
            ut.update(float(i))

        self.assertEqual(160, ut.getLow())
        self.assertEqual(  5, ut.getHigh())

    def testWrapZeroLimits(self):
        ut =  AngleLimits(wrap='0deg')
        ut.update(179.0)
        ut.update(181.0)

        self.assertEqual(179.0, ut.getLow())
        self.assertEqual(181.0, ut.getHigh())


    def testWrap180Limits(self):
        ut =  AngleLimits(wrap='pm180deg')
        ut.update(-0.5)
        ut.update(0.5)

        self.assertEqual(-0.5, ut.getLow())
        self.assertEqual( 0.5, ut.getHigh())

    def testWrap180BigRight(self):
        ut =  AngleLimits(wrap='pm180deg')
        ut.update(355.0)
        for i in range(0, 205, 5):
            ut.update(float(i))

        self.assertEqual(-5, ut.getLow())
        self.assertEqual(-160, ut.getHigh())

    def testWrap180BigLeft(self):
        ut =  AngleLimits(wrap='pm180deg')
        ut.update(5.0)
        for i in range(0, -205, -5):
            ut.update(float(i))

        self.assertEqual(160, ut.getLow())
        self.assertEqual(  5, ut.getHigh())

if __name__ == '__main__':

    unittest.main()

