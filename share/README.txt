To install:

* Checkout to /opt/code-oet with:
git clone <user>@nautilusfs.nautilus:/mnt/nautilusfs/share/repos/code

* Install uctd_envvars.sh into /etc/profile.d

* Copy (or symlink) uctd.desktop to the desktop

* Create /data/current/UCTD.  Mine looks like this:
drwxrwxr-x 5 localadmin users 4096 Apr  5 21:24 /data/current/UCTD/

